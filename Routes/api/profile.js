const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const Profile = require('../../models/Profile');
const User = require('../../models/Users');
const validateProfileInput = require('../../validation/profile');
const validateExperinceInput = require('../../validation/experience');
const validateEducationInput = require('../../validation/education');
const { CountProfileVisits } = require('../../middleWares/ProfileStats');
const { sgMail, msg } = require('../../utils/mail');
const multer = require('multer');
// require puppeteer
const puppeteer = require('puppeteer');
// add elasticsearch for better search functionality 
// connectElasticSearch();
const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client({
  host: process.env.host
});
const searchController = require('../api/elasticsearch');
mongoose.set('useFindAndModify', false);


// creating mapping of database to the elasticCloud 
// searchController.createMapping();

// test route
router.get('/test', (req, res) => {
  res.json({
    success: true,
    msg: 'profile works'
  });
})

// get current profile
//  /api/profile
router.get('/',passport.authenticate('jwt', {session: false}) , (req, res)=>{
  let errors = {};

  Profile.findOne({user: req.user.id})
    .populate('user', ['name', 'avatar'])
    .then((profile)=>{
      if(!profile){
        errors.noprofile = 'Profile not found';
        res.status(404).json(errors);
      } else {
        res.json(profile);
      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json(err);
    });
});

//  create and edit profile
// /api/profile POST private
router.post('/',passport.authenticate('jwt',{session: false}),(req, res)=>{
  const profileFields = {};
  // const { errors, isValid } = validateProfileInput(req.body);  
  // // check if valid
  // if(!isValid){
  //   return res.status(400).json(errors);
  // }
  let errors = {};
  
  profileFields.user = req.user.id;
  if(req.body.handle) profileFields.handle = req.body.handle;
  if(req.body.company) profileFields.company = req.body.company;
  if(req.body.website) profileFields.website = req.body.website;
  if(req.body.location) profileFields.location = req.body.location;
  if(req.body.bio) profileFields.bio = req.body.bio;
  if(req.body.githubUsername) profileFields.githubUsername = req.body.githubUsername;
  if(req.body.status) profileFields.status = req.body.status;
  
  //  skills is a comma seperated value from client side so split it into array 
  if(typeof req.body.skills !== 'undefined'){
    const skills = req.body.skills.split(','); // will return arry
    profileFields.skills = skills;
  }

  // social
  profileFields.social = {};
  if (req.body.youtube) profileFields.social.youtube = req.body.youtube;
  if (req.body.linkedin) profileFields.social.linkedin = req.body.linkedin;
  if (req.body.twitter) profileFields.social.twitter = req.body.twitter;
  if(req.body.instagram) profileFields.social.instagram = req.body.instagram;
  if(req.body.facebook) profileFields.social.facebook = req.body.facebook;

  // EDIT
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      if(profile){
        // update
        Profile.findOneAndUpdate({user: req.user.id}, {$set: profileFields},{new: true})
          .then((profile)=>{
            res.json({success: true, msg: 'updated ', profile: profile});
          })
      } else {
        // create
        
        // check if handle exists
        Profile.findOne({handle: req.user.handle})
          .then((profile)=>{
            // if handle already exists
            if(profile){
              errors.profile = 'handle already exists';
              res.status(400).json(errors);
            } 
            //  else create
            new Profile(profileFields).save()
              .then((profile)=>{
                res.json(profile)
              })
              .catch((err)=>{
                console.log(err);
              });
          });
      }
    })

});


// get profile by handle
//  /api/profile/handle/:handle
// public route

router.get('/handle/:handle',(req, res)=>{
  Profile.findOne({handle: req.params.handle})
    .populate('user', ['name', 'avatar'])
    .then((profile)=>{
      if(!profile){
        res.status(404).json({success: false, msg:'user not found '});
      } else {
        res.json(profile);
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: err});
    });
});


// get profile by userId
//  /api/profile/user/:userId
// public route
// added middleWares for counting the profile visits 

router.get('/user/:userId', CountProfileVisits, (req, res)=>{
  Profile.findOne({user: req.params.userId})
    .populate('user', ['name', 'avatar'])
    .then((profile)=>{
      if(!profile){
        res.status(404).json({success: false, msg:'user not found '});
      } else {
        res.json({success: true, profile: profile});
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: err});
    });
});


// get all profile
//  /api/profile/all

router.get('/all',(req, res)=>{
  Profile.find({})
  .lean()
  .populate('user', ['name', 'avatar'])
    .then((profiles)=>{
      // console.log(profiles);
      // add all the profiles to the elasticsearch
      // client.bulk({body: profiles},(err, response)=>{
      //   if(err){
      //     console.log(err);
      //   }
      // });
      if(!profiles){
        return res.status(404).json({success: false, msg:'no profile found'});
      } else {
        res.status(200).json(profiles);
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: 'server error ' + err});
    });
});

// add experiences
//  /api/profile/experiences
//  private route

router.post('/experience',passport.authenticate('jwt',{session: false}),(req, res)=>{
  const { errors , isValid } = validateExperinceInput(req.body);
  // if(!isValid){
  //   return res.status(400).json({success: false, errors});
  // } 

  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      const newExp = {
        title: req.body.title,
        company: req.body.company,
        location: req.body.location,
        from: req.body.from,
        to: req.body.to,
        description: req.body.description,
        current: req.body.current // for toggling 'To' checkbox     
      };
      profile.experience.unshift(newExp)
      profile.save()
        .then((profile)=>{
          res.json({success: true, msg:'successfully added', profile: profile});
        })
        .catch((err)=>{
          res.status(400).json({success: false, msg: 'server error ' + err});
        });
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: 'server error '+ err});
    });
});


// add education
//  /api/profile/education
//  private route

router.post('/education', passport.authenticate('jwt', {session: false}), (req, res)=>{
  const { errors , isValid } = validateEducationInput(req.body);
  // if(!isValid){
  //   return res.status(400).json({success: false, errors: errors});
  // }

  Profile.findOne({user: req.user.id})
  .populate('user', ['name', 'avatar'])
  .then((profile)=>{
    const newEdu = {
      school: req.body.school,
      degree: req.body.degree,
      from: req.body.from,
      to: req.body.to,
      fieldOfStudy: req.body.fieldOfStudy,
      current: req.body.current,
      description: req.body.description,
    };

    profile.education.unshift(newEdu);
    profile.save()
    .then((profile)=>{
      res.status(200).json({success: true, profile: profile});
    })
    .catch((err)=>{
      res.status(400).json({success: false, error: 'server error '+ err});
    });
  })
  .catch((err)=>{
    res.status(400).json({success: false, error: 'server error ' + err});
  });
})


// Delete experience
//  /api/profile/experience/:expId
//  private route

router.delete('/experience/:expId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
  .populate('user',['name', 'avatar'])
  .then((profile)=>{
    // make an array of id using map
    const indexOfExp = profile.experience.map(item => item._id);
    // find index to remove
    const removableIndex = indexOfExp.indexOf(req.params.expId);
    // now splice out from exp array
    profile.experience.splice(removableIndex,1)
    profile.save().then((profile)=>{
      res.json({success: true, msg: 'deleted experince '+ req.params.expId, profile: profile});
    });
  })
  .catch((err)=>{
    res.status(400).json({success: false, errors: 'server error ' + err});
  });
});


// Delete education
//  /api/profile/education/:eduId
//  private route

router.delete('/education/:eduId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
  .populate('user',['name', 'avatar'])
  .then((profile)=>{
    // make an array of id using map
    const indexOfEdu = profile.education.map(item => item._id);
    // find index to remove
    const removableIndex = indexOfEdu.indexOf(req.params.eduId);
    // now splice out from exp array
    profile.education.splice(removableIndex,1)
    profile.save().then((profile)=>{
      res.json({success: true, msg: 'deleted education '+ req.params.eduId, profile: profile});
    });
  })
  .catch((err)=>{
    res.status(400).json({success: false, errors: 'server error ' + err});
  });
});


// Delete user and profile both
//  /api/profile
//  private route

router.delete('/',passport.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOneAndRemove({user: req.user.id})
    .then(()=>{
      User.findOneAndRemove({_id: req.user.id})
        .then(()=>{
          res.json({success: true, msg: 'profile and user both deleted'});
        })
        .catch((err)=>{
          console.log(err);
        });
    })
    .catch((err)=>{
      return res.status(400).json({success: false, error: 'server error '+ err});
    });
});


// upload image
//  /api/profile/profilePic
// /private route

//  staorage folder with custom filename
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

// type of files allowed
const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10 // 10 mb
  },
  fileFilter: fileFilter
});

router.post('/profilePic',upload.single('profilePic') , passport.authenticate('jwt', {session: false}),(req, res)=>{
  console.log(req.file);
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      if(!profile){
        return res.status(404).json({success: false, msg: 'Profile not found'});
      }
      profile.profilePic = req.file.path;
      profile.save()
        .then((profile)=>{
          // console.log('profile pic uploaded', res);
          res.json({success: true, profile: profile})
        })
        .catch((err)=>{
          console.log(err);
        })
    })
    .catch((err)=>{
      res.status(400).json({success: false, error: err})
    });
});

// follow 
// /api/profile/follow/:followingId
// private

router.post('/follow/:followingUserId',passport.authenticate('jwt',{session: false}), (req, res)=>{
  Profile.find({user: req.user.id})
    .then((profileA)=>{
      if(!profileA){
        return res.status(404).json({success: false, msg: 'profile not found'});
      } else {
        
        // if A follows B
        // then it should add B in the following list of A and also add A in the followers list of B
        // updating A's following list 
        
        profileA.followings.unshift({user: req.params.followingUserId});
        profileA.save()
        .then((result)=>{
        
          // check if userB exists or not till now 
          Profile.findById(req.params.followingUserId)
          .populate('users', ['name'])
          .then((profileB)=>{
            if(!profileB){
              return res.status(404).json({success: false, msg: 'Profile of user not found!'});
            } else {
              // add profileA to the followers of profileB
              profileB.followers.unshift({user: req.user.id});
              profileB.save();
            }
          });
          console.log('after following ', result);
          res.json({success: true, result, msg: `Successfully followed ${req.params.followingUserId}`});
        })
        .catch((err)=>{
          console.log(err);
        })
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: err.errors });
    });
});

// un-follow 
// /api/profile/unfollow/:followingId
// private 

router.post('/unfollow/:followingId', passport.authenticate('jwt', {session: false}), (req, res)=>{
  Profile.find({user: req.user.id})
    .then((profileA)=>{
      if(!profileA){
        return res.status(404).json({success: false, msg: 'Profile not found'});
      } else {
        
        // if A unfollow B 
        //then it should remove B from following of A and remove A from followers of B

        // check if following or not
        const newFollowingArray = profile.followings.filter(followings => followings.user.toString() === req.params.followingId);

        const newFollowingUsers = profile.followings.map(following => following.user);
        const removableIndex = newFollowingUsers.indexOf(req.params.followingId);

        if(Boolean(removableIndex)){
          profileA.followings.splice(removableIndex, 1);
          profileA.save()
          .then((profile)=>{

            // remove A from the followers list of B 
            // check if profileB found 
            Profile.findById(req.params.followingId)
            .then((profileB)=>{
              if(!profileB){
                return res.status(404).json({success: false, msg: `User of id ${req.params.followingId} is not found!`});
              } else {
                
                // remove A from B followers list 
                const followersIds = profileB.followers.map(follower => follower.user);
                const removableFollowerId = followersIds.indexOf(req.user.id);
                
                // check if following or not 
                if(removableFollowerId){
                   profileB.followers.splice(removableFollowerId);
                   profileB.save();
                }
              }
            });
            console.log(profile);
            res.json({success: true, profile, msg : `Successfully unfollowed user of id ${req.params.followingId}`});
          })
          .catch((err)=>{
            console.log(err);
          })
        } else {
          return res.status(404).json({success: false, msg: 'This user is not followed by you'})
        }

      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false, msg: err.errors });
    });
});

// send hire info
// /api/profile/hire/message/:profileId
// private route 

router.post('/hire/message/:profileId', passport.authenticate('jwt',{session: false}), (req, res) =>{
  Profile.findById(req.params.profileId)
    .populate('user',['name', 'email'])
    .then((profile)=>{
      console.log(profile.user);
      console.log(req.user);
      if(!profile){
        return res.status(404).json({success: false, msg: 'profile not found'});
      } else {

        // check message coming from client side  ad send it to the profile holder
        let errors = {};
        if(!Boolean(req.body.senderName)){
          errors.senderName = 'Sender\'s name is required';
        } 
        if(!Boolean(req.body.message)){
          errors.message = 'Message is required';
        } 

        // validate
        if(typeof errors === 'object' && Object.keys(errors).length > 0){
          res.status(400).json({success: false, error: errors })
        } else {
          // prepare to send email 
          msg.to = profile.user.email;
          msg.from = req.user.email;
          msg.subject = `Hiring message from ${req.body.senderName}`;
          msg.text = req.body.message;
          console.log(msg);
          // send 
          sgMail.send(msg,false, (err, response)=>{
            if(err){
              console.log(err);
              res.status(400).json({success: false, error: err });
            }
            res.json({success: true, msg: 'Success'});
          });
        }
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, err });
    });
});


// create pdf 
// /api/profile/pdf/:handle
// public 

router.post('/pdf/:handle',(req, res)=>{
  console.log(`got request for generating pdf for ${req.params.handle}`);
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://dev.to/damcosset/generate-a-pdf-from-html-and-back-on-the-front-5ff5', {
      waitUntil: 'networkidle2'
    });
    const pdf = await page.pdf({
      format: 'A4',
        printBackground: true,
        margin: {
          left: '0px',
          top: '0px',
          right: '0px',
          bottom: '0px'
        }
    });
    await browser.close();
    res.end(pdf)
    })();
});

// creating search functionality for profile
// /api/profile/user/search?handle=:handle
// public

router.get('/user/search',(req, res)=>{
  Profile.findOne({handle: req.query.handle})
  .then((profile)=>{
      if(!profile){
        return res.status(404).json({success: false, msg: `${req.query.handle} not found!`});
      } else {
        console.log(profile);
        res.json({success: true, profile: profile });
      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(400).json({success: false, msg: err.msg });
    });

  // USING ELASTICSEARCH 
  // if(req.query.handle){
  //   const query = req.query.handle.toString().trim();
  //   esClient.search({
  //     index: 'profiles',
  //     body: {
  //       query: {
  //         multi_match: {
  //           query: query,
  //           fields: ["*"],
  //           fuzziness: 2
  //         }
  //       }
  //     }
  //   },(err, results)=>{
  //     if(err){
  //       console.log("Error in searching the user!", err);
  //       return res.status(400).json({success: false, msg: err});
  //     } else {
  //         console.log("Results from user search ", results.hits.hits);
  //         if(results.hits.hits.length == 0){
  //           return res.status(404).json({success: false, msg: `User ${query} is not found`});
  //         } else {
  //           res.status(200).json({success: true, user: results.hits.hits});
  //         }
  //       }
  //   });
  // }
});

// block the user 
router.post('/user/block', passport.authenticate('jwt',  { session: false }), (req, res)=>{
  Profile.findById(req.user._id)
    .then((profile)=>{
      // check if already blocked 
      const blockedUsersId = profile.blockedUser.map(user => user._id);
      const isBlocked = blockedUsersId.indexOf(req.body.blockId);
      if(isBlocked){
        console.log("Already in block list!");
        return res.status(400).json({success: false, msg: "Already in Blocked list!"});
      }
      profile.blockedUser.unshift(req.body.blockId)
      profile.save()
      .then((result)=>{
        return res.status(200).json({success: false , msg: 'Blocked the user!', result});
      })
      .catch((err)=>{
        console.log(err);
        return res.status(400).json({success: false, err });
      });
    })
    .catch((err)=>{
      console.log(err);
      return res.status(404).json({success: false, err });
    });
});


module.exports = router;