const express = require('express');
const router = express.Router();
const passposrt = require('passport');
// load post model
const Post = require('../../models/Posts');
const validatePostInput = require('../../validation/post');
const Profile = require('../../models/Profile')

// test route
router.get('/test', (req, res) => {
  res.json({
    success: true,
    msg: 'posts works'
  });
})

// create post
//  /api/posts/
//  private route
router.post('/',passposrt.authenticate('jwt',{session: false}),(req, res)=>{
  const { errors , isValid } = validatePostInput(req.body);
  // if(!isValid){
  //   return res.status(400).json({success: false, errors: errors});
  // }

  const newPost = {
    text: req.body.text,
    name: req.body.name,
    avatar: req.body.avatar,
    user: req.user.id
  };
  
  new Post(newPost).save()
    .then((post)=>{
      res.json({success: true, post:post})
    })
    .catch((err)=>{
      res.status(400).json({success: false, error: err});
    });
});


// get all posts
//  /api/posts
router.get('/',(req, res)=>{
  Post.find({})
  .sort({date: -1}) // recent first
  .then((posts)=>{
    res.json({success: true, posts: posts});
  })
  .catch((err)=>{
    res.status(404).json({success: false, error: err});
  });
});

// get post by id
//  /api/posts/:id
router.get('/:id',(req, res)=>{
  Post.findById(req.params.id)
  .then((post)=>{
    res.json({success: true, post: post});
  })
  .catch((err)=>{
    res.status(404).json({success: false, error: err});
  });
});

// delete post by id
//  /api/posts/:id
// private route 
router.delete('/:id',passposrt.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      Post.findById(req.params.id)
        .then((post)=>{
          //  check owner
          if(post.user.toString() !== req.user.id){
            return res.status(401).json({success: false, msg:'unauthorized'});
          }
          //  delete
          post.delete()
            .then(()=>{
              res.json({success: true, msg:' deleted '});
            })
        })
        .catch((err)=>{
          res.status(404).json({success: false, msg: 'Post not found'})
        });
    })
});


// like the post
//  /api/posts/like/:postId
// private route 
router.post('/like/:postId',passposrt.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      Post.findById(req.params.postId)
        .then((post)=>{
          // check if already liked
          if(post.likes.filter(like => like.user.toString() === req.user.id ).length > 0){
            console.log('already liked ');
            return res.status(400).json({success: true, msg:'already liked '});
          }
          // else add to likes array 
          post.likes.unshift({user: req.user.id});
          post.save().then((post)=>{
            res.json({success: true, msg: 'liked by '+ req.user.name});
          });
        })
        .catch((err)=>{
          res.status(404).json({success: false, msg: 'Post not found'})
        });
    })
});


//  unlike post 
//  /api/posts/unlike/:postId
// private route
router.post('/unlike/:postId',passposrt.authenticate('jwt',{session: false}),(req, res)=>{
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      Post.findById(req.params.postId)
        .then((post)=>{

          if (post.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
            return res.status(400).json({ notliked: 'You have not yet liked this post' });
          }

            const likedUserIds = post.likes.map((like => like.user.toString()));
            const userIndex = likedUserIds.indexOf(req.user.id);
            post.likes.splice(userIndex, 1);
            post.save()
            .then(()=>{
              console.log('removed like ');
              res.json({success: true, msg: 'removed like '});
            })
            .catch((err)=>{
               res.status(400).json({success: false, err: err});
            })
        })
        .catch((err)=>{
           res.status(400).json({success: false, error: 'Post not found'});
        });
    })
})


// add comment
//  /api/posts/comment/:postId
// private route 

router.post('/comment/:postId',passposrt.authenticate('jwt',{session: false}), (req, res)=>{
  // restricted user to comment if only their profile info exists
  Profile.findOne({user: req.user.id})
    .then((profile)=>{
      Post.findById(req.params.postId)
        .then((post)=>{
          const newComment = {
            text: req.body.text,
            user: req.user.id,
            name: req.user.name,
            avatar: req.user.avatar
          };
          post.comments.unshift(newComment);
          post.save()
          .then((post)=>{
            res.json({success: true, post: post});
          });
        })
        .catch((err)=>{
          res.status(400).json({success: false, error: 'Post not found'});
        });
    })
    .catch((err)=>{
      res.status(404).json({success: false, error: 'Profile not found, please add your profile info first'});
    });
});


// delete comment 
// /api/posts/comment/:postId/:commentId
// private route 

router.delete('/comment/:postId/:commentId',passposrt.authenticate('jwt',{session: false}),(req, res)=>{
  Post.findById(req.params.postId)
    .then((post)=>{
      // check if comment exists
      if(post.comments.filter(comment => comment.id.toString() === req.params.commentId).length === 0){
        return res.status(400).json({success: false, msg: 'Comment does not exists'})
      }
      // if exists find index of comment from array to delete
      const commentIdArray = post.comments.map(comment => comment._id);
      const indexToDelete = commentIdArray.indexOf(req.params.commentId);
      post.comments.splice(indexToDelete, 1);
      post.save()
      .then((post)=>{
        res.json({success: true, post: post});
      });
    })
    .catch((err)=>{
      res.status(404).json({success: false, error: 'Post does not exists '})
    });
})



module.exports = router;