const express = require('express');
const router = express.Router();
const User = require('../../models/Users');
// gravatar for profile pic
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const passport = require('passport');
const validateRegisterInput = require('../../validation/register');
const validLoginInput = require('../../validation/login');


// test route
router.get('/test', (req, res) => {
  res.json({
    success: true,
    msg: 'users works'
  });
})

//  /api/users/register
router.post('/register', (req, res) => {
  //  validate 
  // const { errors , isValid } = validateRegisterInput(req.body);
  // if(!isValid){
  //   return res.status(400).json(errors);
  // }
  User.findOne({
      email: req.body.email
    })
    .then((user) => {
      if (user) {
        res.json({
          success: false,
          msg: 'email already exists'
        });
      } else {
        const avatar = gravatar.url(req.body.email, {
          s: '200', // size
          r: 'pg', // rating
          d: 'mm' // default
        });
        const newUser = new User({
          name: req.body.name,
          email: req.body.email,
          avatar: avatar,
          password: req.body.password
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) {
              res.json({
                success: false,
                error: err
              });
            } else {
              newUser.password = hash;
              newUser.save()
                .then((user) => {
                  res.json({
                    success: true,
                    msg: 'user registered',
                    user: user
                  });
                })
                .catch((err)=>{
                  res.json({success: false, err: err});
                })
            }
          })
        })
      }
    })
})

//  login route /api/users/login
router.post('/login', (req, res)=>{
  const email = req.body.email;
  const password = req.body.password;
  // check user
  User.findOne({email: email})
  .then((user)=>{
    if(!user){
      return res.status(400).json({success: false, email: 'user not found'});
    }
    // compare password
    bcrypt.compare(password, user.password,(err, success)=>{
      if(!success){
        return res.status(400).json({success: false,password: 'incorrect password'});
      }
      if(success){
        // res.json({success: true, msg: 'successfully loggedIn'});
        const payload = { id: user._id, email: user.email, avatar: user.avatar};

        // create token
        jwt.sign(payload, keys.secret, {expiresIn: 3600},(err, token)=>{
          res.json({success: true, token: 'Bearer ' + token});
        })
      }
    })
  })
});

//  get current user
//  /api/users/current
router.get('/current',passport.authenticate('jwt',{ session: false}), (req, res)=>{
  res.json({success: true, msg: 'successfully fetched', id: req.user.id, name: req.user.name, email: req.user.email, gravatar: req.user.avatar });
});

module.exports = router;