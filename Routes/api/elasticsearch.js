const elasticsearch = require('elasticsearch');
const mongoosastic = require('mongoosastic');
const Users = require('../../models/Users');

// CLOUD SETUP 
// esClient 
const esClient = new elasticsearch.Client({
  host: process.env.host
})

// adding the plugin and setting up the elastic cloud  
// options 
const configOptions = {
  host: process.env.host,
  esClient: esClient,
  protocol: "https",
  auth: `${process.env.elasticUserName}:${process.env.elasticPass}`
}

// function to map the mlab data to elastic cloud/elasticsearch
function createMapping(){
  Users.createMapping((err, mapping)=>{
    if(err){
      console.log("Error in mapping the db", err);
    } else {
      console.log("Successfully mapped the data to elasticsearch", mapping)
    }
  });
  // sync the db 
  let streams = Users.synchronize();
  var count = 0;
  // Event driven architecture 
  streams.on("data", ()=>{
    count++;
  });
  streams.on("error",(err)=>{
    console.log(err);
  });
  streams.on("close", ()=>{
    console.log("Total number of indexed docs is ", count);
  });
}


// LOCAL SETUP 
//  const client = new elasticsearch.Client({
//    hosts: ["http://localhost:9200"]
//  });
// const connectElasticSearch = () => {
//   // now create connection 
//   client.ping({ requestTimeout: 30000 }, (err) => {
//       if (err) {
//           console.log('error in hosting elasticsearch ');
//       } else {
//           console.log('elasticssearch worked fine');
//       }
//   });
//    client.indices.create({
//       index: 'profiles'
//   }, (err, res, status) => {
//       if(err){
//           console.log(err);
//       } else {
//           console.log('created new index ', res);
//       }
//   });
// }



module.exports = {
  // client,
  // connectElasticSearch,
  configOptions,
  createMapping
}