# Dev-Hub
### A platform for developers

#### A web app where developers can share their experiences through posts or can showoff their profile and get some opportunity from great organizations.
(SIMILAR TO LINKEDIN) 

## Functionality:-
1. Register/Login
2. Create a profile 
3. Add credentials like education, skills, experience
4. Create posts, like, comment
5. Follow/ unfollow
6. All profiles/ All posts
7. Static and Dynamic caching
8. Send invite to join the company.
9. Download their profile as pdf for resume.
10. Searching the user (as of now) using elasticSearch and mongoosastic for mongoDB as a plugin.
11. Hire the developers

## Tech Stacks used :
1. Node.js
2. Express.js
3. MongoDB
4. React.js
5. ElasticSearch
6. Redux

## How to run:

> __For running only backend server.__
```sh 
1. First clone the repo and "cd" to the cloned directory.
2. Install the dependencies using "npm install" 
3. Now use command "npm run server"
4. Now the server is running on port 5000.
```

> __For running only client-side.__

```sh
1. First clone the repo and "cd" to the cloned directory.
2. Install the dependencies using "npm install" 
3. Now use command "npm run client"
4. Now the client side is running on port 3000.
```

## Prerequisite

1. Add a .env file in the root of the project and include the given below information in that file.

```sh
mongoUri = '<YOUR_MONGO_URL>'
PORT = 5000
secret ='<YOUR_SECRET_KEY>'
mailService = "SendGrid"
sendgridUsername = "<YOUR_SENDGRID_USERNAME>"
sendgridPassword = "<YOURSENDGRID_PASSWORD>"
emailFrom = "<YOUR_EMAIL_ADDRESS>(Used for sending invite to join company)"
SENDGRID_API_KEY = '<YOUR_SENDGRID_API_KEY>'
elasticPass = "<YOUR_ELASTICSEARCH_CLOUD_PASSWORD>"
elasticUsername = "<YOUR_ELASTICSEARCH_CLOUD_USERNAME>"
host = "<YOUR_ELASTICSEARCH_CLOUD_HOST_URL>"
```
 
**Note:**

>1. If you don't add your email address and sendgrid username and password in .env as given above, sending invite functionality may not work.

>2. If you don't add elasticSearch cloud username and password then user search functionality may not work, but alternatively you can use your locally setup elasticSearch server and make changes accordingly.

## Demo

![Landing](https://user-images.githubusercontent.com/31209617/91832307-e029b000-ec62-11ea-8c24-79d97e4b4043.PNG)
![Sign](https://user-images.githubusercontent.com/31209617/91832349-ed469f00-ec62-11ea-8756-233d96a9ec72.PNG)
![log](https://user-images.githubusercontent.com/31209617/91832355-efa8f900-ec62-11ea-9ea7-b745ac2da9f7.PNG)
![First](https://user-images.githubusercontent.com/31209617/91832366-f33c8000-ec62-11ea-8a7c-ec24f1084711.PNG)
![create3](https://user-images.githubusercontent.com/31209617/91832390-fb94bb00-ec62-11ea-8b39-c12e1da1bf31.PNG)
![AddExp](https://user-images.githubusercontent.com/31209617/91832398-ff284200-ec62-11ea-9a0e-6bda52f5514f.PNG)
![AddEdu](https://user-images.githubusercontent.com/31209617/91832407-00596f00-ec63-11ea-953c-ccaa70b95725.PNG)
![Devs](https://user-images.githubusercontent.com/31209617/91832421-03ecf600-ec63-11ea-9d70-7f712a70cfbb.PNG)
![profile](https://user-images.githubusercontent.com/31209617/91832447-0b140400-ec63-11ea-91f9-8a20fe95f375.PNG)
![Posts](https://user-images.githubusercontent.com/31209617/91832458-0d765e00-ec63-11ea-83f2-44dba9981208.PNG)
![Hire](https://user-images.githubusercontent.com/31209617/91832505-1b2be380-ec63-11ea-9747-3ea1a2fa9b6e.PNG)



## Build with :heart: & :headphones: & :computer: by Rupeshiya.

