const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profileViewedSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  noOfViews: {
    type: Number,
    default: 0
  },
  visiters: [
    {
      visiter: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'users'
      }
    }
  ]
});

module.exports = mongoose.model('ProfileStats', profileViewedSchema);