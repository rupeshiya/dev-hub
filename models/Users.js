const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');
const elasticsearch = require('elasticsearch');
const options = require('../Routes/api/elasticsearch');

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

userSchema.plugin(mongoosastic, options.configOptions);

module.exports = user = mongoose.model('users', userSchema);