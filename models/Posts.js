const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosastic = require('mongoosastic');
const elasticsearch = require('elasticsearch');
const options = require('../Routes/api/elasticsearch');

const postSchema = new Schema({
  user:{
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  text:{
    type: String,
    required: true
  },
  name:{
    type: String
  },
  avatar:{
    type: String
  },
  likes:[
    {
      user:{
        type: Schema.Types.ObjectId,
        ref: 'users'
      }
    }
  ],
  comments:[
    {
      user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
      name:{
          type: String
      },
      avatar: {
        type: String
      },
      text:{
        type: String,
        required: true
      },
      date:{
        type: Date,
        default: Date.now()
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now()
  }
});

postSchema.plugin(mongoosastic, options.configOptions);
module.exports = Post = mongoose.model('post', postSchema);
