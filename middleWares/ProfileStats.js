const mongoose = require('mongoose');
const ProfileVisitStatsModel = require('../models/ProfileVisits');

module.exports = {
  CountProfileVisits: (req, res, next) => {
    // check if already visited
    const profileVisitId = req.params.userId;
    ProfileVisitStatsModel.findById(profileVisitId)
      .populate('user', ['name', 'email'])
      .select('noOfViews visiters user')
      .exec((err, result) => {
        if (err) {
          console.log(err);
          next(); // should not interrupt the whole functionality of that route 
        } else {
          // check if already visited the same profile previously 
          const profileVisitersId = result.visiters.map(visiter => visiter._id);
          const isAlreadyVisited = profileVisitersId.indexOf(req.user._id);
          if (isAlreadyVisited) {
            console.log("Already visited!");
            next();
          } else {
            // add to the visiters and incr the count 
            result.visiters.unshift(req.user._id);
            result.noOfViews += 1;
            result.save()
              .then((pro) => {
                console.log("After successfully addition visiters ", pro);
                next();
              });
          }
        }
      })
  }
}