/* eslint-disable no-restricted-globals */
var CACHE_NAME = 'DEV-HUB_1';
var DYNAMIC_CACHE = 'DYNAMIC_DEV_HUB_1'
var urlToCache = [
  '/',
  '/login',
  '/dashboard',
  '/profiles'
];

self.addEventListener('install',(event)=>{
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        console.log('installing worker');
        console.log('cache opened');
        return cache.addAll(urlToCache);
      })
      .then(()=>{ self.skipWaiting()})
  )
});

// Cache and return requests
// self.addEventListener('fetch', event => {
//   event.respondWith(
//     caches.match(event.request)
//     .then(function (response) {
//       console.log('from cacahe',response);
//       // Cache hit - return response
//       if (response) {
//         return response;
//       }
//       // fetch request 
//       var requestToFetch = event.request.clone();
//       return fetch(requestToFetch).then((response)=>{
//         if(!response || response.status !== 200) {
//           return response;
//         }
//         // if response then cache it
//         var responseToClone = response.clone();
//           caches.open(DYNAMIC_CACHE)
//             .then((cache) => {
//               cache.put(event.request, responseToClone);
//             })
//           return response;
//       })
//     })
//   );
// });

// Update a service worker
self.addEventListener('activate', event => {
  var cacheWhitelist = ['DEV-HUB'];
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          if (cacheWhitelist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});