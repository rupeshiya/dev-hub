import React from 'react';
import './App.css';
import Header from './components/layouts/Header';
import Footer from './components/layouts/Footer';
import Landing from './components/layouts/Landing';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import { Provider } from 'react-redux';
import store from './store';
import jwt_decode from 'jwt-decode';
import { setAuthToken } from './utils/setAuthToken';
import { setCurrentUser } from './actions/authAction';
import { logoutUser } from './actions/authAction';
import Dashboard from './components/dashboard/Dashboard';
import { clearCurrentProfile } from './actions/profileAction';
import PrivateRoute from './common/PrivateRoute';
import CreateProfile from './components/createProfile/CreateProfile';
import EditProfile from './components/edit-profile/EditProfile';
import AddExperience from './components/add-credentials/AddExperience';
import AddEducation from './components/add-credentials/AddEducation';
import Profiles from './components/Profiles/Profiles';
import Profile from './components/Profile/Profile';
import Posts from './components/Posts/Posts';
import Post from './components/Post/Post';
import 'react-notifications/lib/notifications.css';
import PageProgress  from 'react-page-progress';


// check for token
if(localStorage.jwtToken){

  // get user data from token 
  const decoded = jwt_decode(localStorage.jwtToken);
  
  // set authorization in headert for all the request 
  setAuthToken(localStorage.jwtToken);

  // set user and isAuthenticated true
  store.dispatch(setCurrentUser(decoded));

  // check if token expired
  const currentTime = Date.now() / 1000; // in ms
  if(decoded.exp < currentTime){
    // logout user
    store.dispatch(logoutUser());
    
    // clear current profile
    store.dispatch(clearCurrentProfile());

    // redirect to login
    window.location.href = "/login";
  } 
}


function App() {
  return (
    <Provider store={store}>
      <Router>
      <div className="App">
        <Header />
        {/* added progress bar using react-page-progress component  */}
          <PageProgress color={'skyblue'} height={5} />
          <Route exact path="/" component={Landing} />
          <div className="container">
            <Switch>
              <Route exact path="/register" component={Register} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/profiles" component={Profiles} />
              <PrivateRoute exact path="/dashboard" component={Dashboard} />
              <PrivateRoute exact path="/create-profile" component={CreateProfile} />
              <PrivateRoute exact path="/edit-profile" component={EditProfile} />
              <PrivateRoute exact path="/add-experience" component={AddExperience} />
              <PrivateRoute exact path = "/add-education" component={AddEducation} />
              <PrivateRoute exact path = "/profile/:handle" component={Profile} />
              <PrivateRoute exact path = "/posts" component={Posts} />
              <PrivateRoute exact path = "/post/:postId" component={Post} />
            </Switch>
          </div>
      <Footer />
      </div>
      </Router>
    </Provider>
  );
}

export default App;
