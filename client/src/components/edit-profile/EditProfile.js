import React, { Component } from 'react';
import { connect } from 'react-redux';
import  TextFieldGroup  from '../../common/TextFieldGroup';
import SelectField from '../../common/SelectField';
import  SocialInputGroup  from '../../common/SocialInputGroup';
import  TextAreaField  from '../../common/TextAreaField';
import { createProfile, getCurrentProfile , uploadImage} from '../../actions/profileAction';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';



class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleSocialInput: false,
      handle: '',
      company: '',
      website: '',
      location: '',
      status: '',
      skills: '',
      githubUsername: '',
      bio: '',
      twitter: '',
      facebook: '',
      linkedin: '',
      youtube: '',
      instagram: '',
      selectedFile: null,
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentProfile();
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.errors){
      this.setState({errors: nextProps.errors});
    }
    // check if got profile
    if(nextProps.profile.profile){
      const profile = nextProps.profile.profile;
      console.log(profile);
      //  check if empty then set it to a string
      const skillsCsv = profile.skills.join(',');
      profile.company = Boolean(profile.company) ? profile.company : '';
      profile.website = Boolean(profile.website) ? profile.website : '';
      profile.bio = Boolean(profile.bio) ? profile.bio : '';
      profile.location = Boolean(profile.location) ? profile.location : '';
      profile.githubUsername = Boolean(profile.githubUsername) ? profile.githubUsername : '';
      // profile.social = Boolean(Object.keys(profile.social).length > 0) ? profile.social : {};
      // check for individual keys of social
      if(Boolean(profile.social)){
        profile.youtube = Boolean(profile.social.youtube) ? profile.social.youtube : '';
        profile.twitter = Boolean(profile.social.twitter) ? profile.social.twitter : '';
        profile.instagram = Boolean(profile.social.instagram) ? profile.social.instagram : '';
        profile.facebook = Boolean(profile.social.facebook) ? profile.social.facebook : '';
        profile.linkedin = Boolean(profile.social.linkedin) ? profile.social.linkedin : '';
      } else {
        profile.youtube = '';
        profile.twitter = '';
        profile.instagram = '';
        profile.linkedin = '';
        profile.facebook = '';
      }


      //  set fields values in form
      this.setState({
          handle: profile.handle,
           company: profile.company,
           website: profile.website,
           location: profile.location,
           status: profile.status,
           skills: skillsCsv,
           githubUsername: profile.githubUsername,
           bio: profile.bio,
           twitter: profile.twitter,
           facebook: profile.facebook,
           linkedin: profile.linkedin,
           youtube: profile.youtube,
           instagram: profile.instagram
      })
    }
  }

  onSubmit = (e) =>{
    e.preventDefault();
    console.log('create profile submitted');

    // user profile data 
    const profileData = {
      handle: this.state.handle,
      company: this.state.company,
      website: this.state.website,
      location: this.state.location,
      status: this.state.status,
      skills: this.state.skills,
      githubUsername: this.state.githubUsername,
      bio: this.state.bio,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
      youtube: this.state.youtube,
      instagram: this.state.instagram
    };
    console.log(profileData);
    this.props.createProfile(profileData, this.props.history);
  }

  onChange = (e) =>{
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value})
  }
  
  toggleSocial = () =>{
    this.setState({toggleSocialInput: !this.state.toggleSocialInput});
  }
  fileHandler = (event) => {
    console.log(event.target.files[0]);
    this.setState({selectedFile: event.target.files[0]});
  }
  //  upload to the server
  uploadImage = (event) => {
    event.preventDefault();
    console.log('upload clicked');
    const newImageData = new FormData();
    newImageData.append('profilePic', this.state.selectedFile);
    // newImageData(this.state.selectedFile);
    this.props.uploadImage(newImageData);
  }

  render() {
    let socialInputs = (
      <div className="form-group">
        <SocialInputGroup 
         placeholder="Twitter Profile URL"
          name="twitter"
          className="form-control form-group-lg"
          icon="fab fa-twitter"
          value={this.state.twitter}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Facebook Page URL"
          name="facebook"
          className="form-control form-group-lg"
          icon="fab fa-facebook"
          value={this.state.facebook}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Linkedin Profile URL"
          name="linkedin"
          className="form-control form-group-lg"
          icon="fab fa-linkedin"
          value={this.state.linkedin}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="YouTube Channel URL"
          name="youtube"
          className="form-control form-group-lg"
          icon="fab fa-youtube"
          value={this.state.youtube}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Instagram Page URL"
          name="instagram"
          className="form-control form-group-lg"
          icon="fab fa-instagram"
          value={this.state.instagram}
          onChange={this.onChange}
        />
      </div>
    )

    const options = [
      { label: 'Select your professional status' },
      { label: 'CEO', value:'CEO' },
      { label: 'CTO', value: 'CTO' },
      { label: 'Sr. manager', value: 'Sr. manager' },
      { label: 'Sr. Developer', value: 'Sr. Developer' },
      { label: 'Jr. Developer', value: 'Jr. Developer' },
      { label: 'Jr. Manager', value: 'Jr. Manager' },
      { label: 'Student', value: 'Student' },
      { label: 'Others', value: 'Others' }
    ];
    const { toggleSocialInput } = this.state;
    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
               <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Edit profile</h1>
              <small className="text text-muted">*(= required)</small>
                <form onSubmit={this.onSubmit}>
                <TextFieldGroup 
                  placeholder="* Profile Handle"
                  name="handle"
                  className="form-control form-group-lg"
                  value={this.state.handle}
                  onChange={this.onChange}
                  info="A unique handle for your profile URL. Your full name, company name, nickname"/>
                  <SelectField
                    placeholder="Status"
                    name="status"
                    className = "form-control form-group-lg"
                    value={this.state.status}
                    onChange={this.onChange}
                    options={options}
                    info = "Give us an idea of where you are at in your career"
                  />
                <TextFieldGroup 
                  placeholder="Company"
                  name="company"
                  className = "form-control form-group-lg"
                  value={this.state.company}
                  onChange={this.onChange}
                  info = "Could be your own company or one you work for"
                />
                <TextFieldGroup 
                  placeholder="Website"
                  name="website"
                  className = "form-control form-group-lg"
                  value={this.state.website}
                  onChange={this.onChange}
                  info = "Could be your own website or a company one"
                />
                <TextFieldGroup 
                  placeholder="Location"
                  name="location"
                  className = "form-control form-group-lg"
                  value={this.state.location}
                  info = "City or city & state suggested (eg. Bhubaneswar, India)"
                  onChange={this.onChange}
                />
                <TextFieldGroup
                  placeholder="* Skills"
                  name="skills"
                  className = "form-control form-group-lg"
                  value={this.state.skills}
                  onChange={this.onChange}
                  // error={errors.skills}
                  info="Please use comma separated values (eg.
                  Golang,JavaScript,BrainFuck"
                />
                <TextFieldGroup 
                  placeholder="Github username (eg. Rupeshiya)"
                  name="githubUsername"
                  className = "form-control form-group-lg"
                  value={this.state.githubUsername}
                  onChange={this.onChange}
                  info = "If you want your latest repos and a Github link, include your username"
                />
                <TextAreaField 
                  placeholder="Short bio"
                  name="bio"
                  className = "form-control form-group-lg"
                  value={this.state.bio}
                  onChange={this.onChange}
                  info = "Tell us a little about yourself"
                />
                <div className="form-group">
                  <button type="button" className="btn btn-block btn-outline-light" style={{color:'blue', borderRadius:'5px', borderColor: 'blue'}} onClick={this.toggleSocial}>Social links (optional) {toggleSocialInput ? '(Hide)': '(Show)'}</button>
                </div>
                  {toggleSocialInput ? socialInputs : null}
                  <div className="form-group">
                      <input type="file" onChange={this.fileHandler} className="form-control form-control-lg" name="profilePic"/>
                      <button type="button" className="btn btn-block btn-outline-success" onClick={this.uploadImage}>Upload Profile Pic</button>
                  </div>
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
                </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// validate props
EditProfile.propType = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  createProfile: PropTypes.func.isRequired,
  getCurrentProfile: PropTypes.func.isRequired
}
// map state to props 
const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps ,{createProfile, getCurrentProfile, uploadImage})(withRouter(EditProfile));
