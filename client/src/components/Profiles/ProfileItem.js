import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Modal from '../../common/modals';


class ProfileItem extends Component {
  state = {
    isFollowing: false
  }

  onHireClick = (profileId ) => {
    console.log('hire clicked ', profileId);
  }
  onFollow = (userId) => {
    console.log(userId);
  }
  onUnFollow = (userId) => {
    console.log(userId);
  } 

  componentWillReceiveProps(nextProps){
    console.log(nextProps);
    const followingArray = nextProps.profile.followings;

  }

  render() {
    const { profile } = this.props;
    return (
      <div className="card card-body bg-light mb-3">
          <div className="row">
            <div className="col-2">
              {Boolean(profile.user) ? (<img src={profile.user.avatar} alt="" className="rounded-circle" />): (<img src='https://cdn2.vectorstock.com/i/1000x1000/17/61/male-avatar-profile-picture-vector-10211761.jpg' alt="" className="rounded-circle" />)}
            </div>
            <div className="col-lg-6 col-md-4 col-8">
              <h3>{Boolean(profile.user) ? profile.user.name : 'Anonymous'}</h3>
              <p>
                {profile.status}{' '}
                {Boolean(profile.company) ?  (
                  <span>at {profile.company}</span>
                ): null }
              </p>
              <p>{Boolean(profile.location) ? (profile.location) : null}</p>
                <div className="row">
                  <Link to={`/profile/${profile.handle}`} className="btn btn-info">
                  View Profile
                  </Link>
                  <button className="btn btn-success ml-3" data-toggle="modal" data-target="#exampleModal" onClick={this.onHireClick.bind(this, profile._id)}>Hire {Boolean(profile.user) ? profile.user.name : 'Anonymous'}
                  </button>
                  {
                  Boolean(this.state.isFollowing) ? 
                  (<button className="btn btn-sm btn-outline-danger" onClick={this.onUnFollow(profile.user._id)}>
                    Unfollow
                  </button>) :
                  (<button className="btn btn-sm btn-outline-light" onClick={this.onFollow(profile.user._id)}>Follow</button>)
                  }
                  <Modal profileId={profile._id}/>
                </div>
            </div>
            <div className="col-md-4 d-none d-md-block">
              <h4>Skill Set</h4>
              <ul className="list-group">
                {profile.skills.slice(0, 4).map((skill, index) => (
                  <li key={index} className="list-group-item">
                    <i className="fa fa-check pr-1" />
                    {skill}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
    )
  }
}
//  validate props 
ProfileItem.protoTypes = {
  profile: PropTypes.object.isRequired
}
export default ProfileItem;