import React, { Component } from 'react';
import { connect } from 'react-redux';
import  TextFieldGroup  from '../../common/TextFieldGroup';
import SelectField from '../../common/SelectField';
import  SocialInputGroup  from '../../common/SocialInputGroup';
import  TextAreaField  from '../../common/TextAreaField';
import { createProfile } from '../../actions/profileAction';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggleSocialInput: false,
      handle: '',
      company: '',
      website: '',
      location: '',
      status: '',
      skills: '',
      githubUsername: '',
      bio: '',
      twitter: '',
      facebook: '',
      linkedin: '',
      youtube: '',
      instagram: '',
      errors: {}
    };
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.errors){
      this.setState({errors: nextProps.errors});
    }
  }

  onSubmit = (e) =>{
    e.preventDefault();
    console.log('create profile submitted');

    // user profile data 
    const profileData = {
      handle: this.state.handle,
      company: this.state.company,
      website: this.state.website,
      location: this.state.location,
      status: this.state.status,
      skills: this.state.skills,
      githubUsername: this.state.githubUsername,
      bio: this.state.bio,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      linkedin: this.state.linkedin,
      youtube: this.state.youtube,
      instagram: this.state.instagram
    };
    console.log(profileData);
    this.props.createProfile(profileData, this.props.history);
  }

  onChange = (e) =>{
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value})
  }
  
  toggleSocial = () =>{
    this.setState({toggleSocialInput: !this.state.toggleSocialInput});
  }
  
  render() {
    let socialInputs = (
      <div className="form-group">
        <SocialInputGroup 
         placeholder="Twitter Profile URL"
          name="twitter"
          className="form-control form-group-lg"
          icon="fab fa-twitter"
          value={this.state.twitter}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Facebook Page URL"
          name="facebook"
          className="form-control form-group-lg"
          icon="fab fa-facebook"
          value={this.state.facebook}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Linkedin Profile URL"
          name="linkedin"
          className="form-control form-group-lg"
          icon="fab fa-linkedin"
          value={this.state.linkedin}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="YouTube Channel URL"
          name="youtube"
          className="form-control form-group-lg"
          icon="fab fa-youtube"
          value={this.state.youtube}
          onChange={this.onChange}
        />
        <SocialInputGroup 
          placeholder="Instagram Page URL"
          name="instagram"
          className="form-control form-group-lg"
          icon="fab fa-instagram"
          value={this.state.instagram}
          onChange={this.onChange}
        />
      </div>
    )

    const options = [
      { label: 'Select your professional status' },
      { label: 'CEO', value:'CEO' },
      { label: 'CTO', value: 'CTO' },
      { label: 'Sr. manager', value: 'Sr. manager' },
      { label: 'Sr. Developer', value: 'Sr. Developer' },
      { label: 'Jr. Developer', value: 'Jr. Developer' },
      { label: 'Jr. Manager', value: 'Jr. Manager' },
      { label: 'Student', value: 'Student' },
      { label: 'Others', value: 'Others' }
    ];
    const { toggleSocialInput } = this.state;
    return (
      <div className="create-profile">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Create profile</h1>
              <p className="lead text-center">Let's begin your awesome journey here</p>
              <small className="text text-muted">*(= required)</small>
                <form onSubmit={this.onSubmit}>
                <TextFieldGroup 
                  placeholder="* Profile Handle"
                  name="handle"
                  className="form-control form-group-lg"
                  value={this.state.handle}
                  onChange={this.onChange}
                  info="A unique handle for your profile URL. Your full name, company name, nickname"/>
                  <SelectField
                    placeholder="Status"
                    name="status"
                    className = "form-control form-group-lg"
                    value={this.state.status}
                    onChange={this.onChange}
                    options={options}
                    info = "Give us an idea of where you are at in your career"
                  />
                <TextFieldGroup 
                  placeholder="Company"
                  name="company"
                  className = "form-control form-group-lg"
                  value={this.state.company}
                  onChange={this.onChange}
                  info = "Could be your own company or one you work for"
                />
                <TextFieldGroup 
                  placeholder="Website"
                  name="website"
                  className = "form-control form-group-lg"
                  value={this.state.website}
                  onChange={this.onChange}
                  info = "Could be your own website or a company one"
                />
                <TextFieldGroup 
                  placeholder="Location"
                  name="location"
                  className = "form-control form-group-lg"
                  value={this.state.location}
                  info = "City or city & state suggested (eg. Bhubaneswar, India)"
                  onChange={this.onChange}
                />
                <TextFieldGroup
                  placeholder="* Skills"
                  name="skills"
                  className = "form-control form-group-lg"
                  value={this.state.skills}
                  onChange={this.onChange}
                  // error={errors.skills}
                  info="Please use comma separated values (eg.
                  Golang,JavaScript,BrainFuck"
                />
                <TextFieldGroup 
                  placeholder="Github username (eg. Rupeshiya)"
                  name="githubUsername"
                  className = "form-control form-group-lg"
                  value={this.state.githubUsername}
                  onChange={this.onChange}
                  info = "If you want your latest repos and a Github link, include your username"
                />
                <TextAreaField 
                  placeholder="Short bio"
                  name="bio"
                  className = "form-control form-group-lg"
                  value={this.state.bio}
                  onChange={this.onChange}
                  info = "Tell us a little about yourself"
                />
                <div className="form-group">
                  <button type="button" className="btn btn-block btn-outline-light" style={{color:'blue', borderRadius:'5px', borderColor: 'blue'}} onClick={this.toggleSocial}>Social links (optional) {toggleSocialInput ? '(Hide)': '(Show)'}</button>
                </div>
                  {toggleSocialInput ? socialInputs : null}
                <input
                  type="submit"
                  value="Submit"
                  className="btn btn-info btn-block mt-4"
                />
                </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// validate props
CreateProfile.propType = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  createProfile: PropTypes.func.isRequired
}
// map state to props 
const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps ,{createProfile})(withRouter(CreateProfile));
