import React, { Component } from 'react'
import Moment from 'react-moment';

class ProfileCreds extends Component {
  render() {
    const { education ,experience } = this.props;
    let expItems;
    let eduItems;
    if(Boolean(experience)){
     expItems = experience.map(exp => (
        <li key={exp._id} className="list-group-item">
          <h4>{exp.company}</h4>
          <p>
            <Moment format="YYYY/DD/MM">{' ' + exp.from }</Moment> - {Boolean(exp.to) ? (<Moment format="YYYY/DD/MM">{exp.to}</Moment>) : ' Now'}
          </p>

        <p>
          <strong>Position:</strong> {exp.title}
        </p>
          <p><strong>Description:</strong> {Boolean(exp.description) ? (exp.description) : null}</p>
        </li>
      ))
    }
    if(Boolean(education)){
    eduItems = education.map(edu =>(
        <li className="list-group-item" key={edu._id}>
          <h4>{edu.school}</h4>
        <p><strong>Degree:</strong>{' ' + edu.degree + ' '} ({edu.fieldOfStudy})</p>
          <p>
            <Moment format="YYYY/DD/MM">{edu.from}</Moment> - {Boolean(edu.to) ? (<Moment format="YYYY/DD/MM">{edu.to}</Moment>) : 'Now'}
          </p>
          <p><strong>Description:</strong> {Boolean(edu.description) ? (edu.description) : null}</p>
        </li>
      ))
    }
    return (
      <div className="row">
        <div className="col-md-6">
          <h3 className="text-center text-info">Experience</h3>
          {expItems.length > 0 ? (
            <ul className="list-group">{expItems}</ul>
          ) : (
            <p className="text-center">No Experience Listed</p>
          )}
        </div>

        <div className="col-md-6">
          <h3 className="text-center text-info">Education</h3>
          {eduItems.length > 0 ? (
            <ul className="list-group">{eduItems}</ul>
          ) : (
            <p className="text-center">No Education Listed</p>
          )}
        </div>
      </div>
    )
  }
}
export default ProfileCreds;