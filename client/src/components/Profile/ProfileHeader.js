import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class ProfileHeader extends Component {
  render() {
    const { profile } = this.props;
    let socilaLinks;
    if(Boolean(profile.social)){
      if(Object.keys(profile.social).length > 0){
        socilaLinks = (
          <div>
            <a className="text-white p-2" href={Boolean(profile.social.youtube) ? (profile.social.youtube) : '#'} target="_blank" rel="noopener noreferrer">
              <i className="fas fa-globe fa-2x"></i>
            </a>
            <a className="text-white p-2" href={Boolean(profile.social.twitter) ? (profile.social.twitter) : '#'} target="_blank" rel="noopener noreferrer">
              <i className="fab fa-twitter fa-2x"></i>
            </a>
            <a className="text-white p-2" href={Boolean(profile.social.twitter) ? (profile.social.facebook) : '#'} target="_blank" rel="noopener noreferrer">
              <i className="fab fa-facebook fa-2x"></i>
            </a>
            <a className="text-white p-2" href={Boolean(profile.social.linkedin) ? (profile.social.linkedin) : '#'} target="_blank" rel="noopener noreferrer"> 
              <i className="fab fa-linkedin fa-2x"></i>
            </a>
            <a className="text-white p-2" href={Boolean(profile.social.instagram) ? (profile.social.instagram) : '#'} target="_blank" rel="noopener noreferrer">
              <i className="fab fa-instagram fa-2x"></i>
            </a>
          </div>
        )
      }
    }

    return (
       <div className="row">
            <div className="col-md-12">
              <div className="card card-body bg-info text-white mb-3">
                <div className="row">
                  <div className="col-4 col-md-3 m-auto">
                      {Boolean(profile.user) ? (<img src={profile.user.avatar} alt="" className="rounded-circle" />): (<img src='https://cdn2.vectorstock.com/i/1000x1000/17/61/male-avatar-profile-picture-vector-10211761.jpg' alt="" className="rounded-circle" />)}
                  </div>
                </div>
                <div className="text-center">
                  <h1 className="display-4 text-center">{Boolean(profile.user) ? profile.user.name : 'Anonymous'}</h1>
                  <p className="lead text-center">{profile.status + ' '}<span>At {Boolean(profile.company) ? (profile.company) : null }</span></p>
                  <p>{Boolean(profile.location) ? (profile.location) : null}</p>
                  <div>
                    {socilaLinks}
                  </div>
                </div>
              </div>
            </div>
          </div>
    )
  }
}
// validate props 
ProfileHeader.protoTypes = {
  profile: PropTypes.object.isRequired
}

export default ProfileHeader;