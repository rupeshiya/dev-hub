import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getProfileByHandle } from '../../actions/profileAction';
import ProfileHeader from './ProfileHeader';
import ProfileAbout from './ProfileAbout';
import ProfileCreds from './ProfileCreds';
import ProfileGit from './ProfileGit';
import Spinner from '../../common/Spinner';
import { saveAs } from 'file-saver';
// import { getProfilePdf } from '../../actions/profileAction';


class Profile extends Component {
  
  componentDidMount(){
    if(this.props.match.params.handle){
      this.props.getProfileByHandle(this.props.match.params.handle);    
    }
  }

  saveAspdf = () => {
    const { profile } = this.props.profile;
    // todo 
    // change the url after deployment
      fetch(`/api/profile/pdf/${profile.handle}`, {
          method: 'POST'
      }).then(res => {
          return res
              .arrayBuffer()
              .then(res => {
                  const blob = new Blob([res], { type: 'application/pdf' })
                  saveAs(blob, `${profile.handle}.pdf`)
              })
              .catch(e => alert(e))
      })
    }


  render() {

    const { profile, isLoading } = this.props.profile;
    console.log(profile);
    let profileContent;
    
    if(profile === null || isLoading){
      profileContent = <Spinner />
    } else {
      profileContent = (
        <div>
          <div className="row">
            <div className="col-md-6">
              <Link to="/profiles" className="btn btn-light mb-3 float-left">
                Back To Profiles
              </Link>
               <button className="btn btn-light ml-3" onClick={this.saveAspdf}>Download as pdf</button>
            </div>
            <div className="clearfix"></div>
          </div>
          <ProfileHeader profile={profile} />
          <ProfileAbout profile={profile} />
          <ProfileCreds
            education={profile.education}
            experience={profile.experience}
          />
          {Boolean(profile.githubUsername) ? (
            <ProfileGit username={profile.githubUsername} />
          ) : null}
        </div>
      )
    }
    return (
      <div className="profile">
         <div className="profile">
        <div className="container">
          <div className="row">
            <div className="col-md-12">{profileContent}</div>
          </div>
        </div>
      </div>
      </div>
    )
  }
}

// validate props 
Profile.protoTypes = {
  profile: PropTypes.object.isRequired,
  getProfileByHandle: PropTypes.func.isRequired,
  getProfilePdf: PropTypes.func.isRequired
}

// map state to props 
const mapStateToProps = (state) => ({
  profile: state.profile,
  error: state.errors
});

export default connect(mapStateToProps, { getProfileByHandle })(Profile);