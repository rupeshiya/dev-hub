import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authAction';
import PropTypes from 'prop-types';
import TextFieldGroup from '../../common/TextFieldGroup';
import { withRouter } from 'react-router-dom';

class Login extends Component {
  constructor(){
    super();
    this.state = {
      email: '',
      password: '',
      errors: {}
    };
  }

  // restrict to comeback if authenticated
  componentDidMount(){
    if(this.props.auth.isAuthenticated){
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps){
    // if authenticated redirect to dashboard
    if(nextProps.auth.isAuthenticated){
      this.props.history.push('/dashboard');
    }
    // if error
    if(nextProps.errors){
      this.setState({ errors: nextProps.errors})
    }
  }

  onChange = (e) =>{
    this.setState({[e.target.name]: e.target.value});
  }
  
  onSubmit = (e) =>{
    e.preventDefault();
    const loginInfo = {
      email: this.state.email,
      password: this.state.password
    };
    console.log(loginInfo);
    this.props.loginUser(loginInfo);
    // loginUser(loginInfo);
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="login">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Log In</h1>
              <p className="lead text-center">Sign in to your DevConnector account</p>
              <ul className="list-unstyled">
                <li className="text text-danger">
                  {Boolean(errors.msg) ? errors.msg : null }
                </li>
              </ul>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup 
                  className="form-control form-control-lg"
                   type="email"
                   placeholder="Email Address"
                   name="email"
                   value={this.state.email}
                   onChange={this.onChange}
                />
                <TextFieldGroup 
                  className="form-control form-control-lg"
                   type="password"
                   placeholder="Password"
                   name="password"
                   value={this.state.password}
                   onChange={this.onChange}
                />
                <input type="submit" className="btn btn-info btn-block mt-4" disabled={!this.state.email || !this.state.password}/>
              </form>
            </div>
          </div>
        </div>
    </div>  
    )
  }
}
Login.protoType = {
  auth: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
}

// map state to props
const mapStateToProps = (state) =>({
  auth: state.auth,
  errors: state.errors
})

export default connect(mapStateToProps, { loginUser })(withRouter(Login));