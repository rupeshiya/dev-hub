import React, { Component } from 'react';
import classnames from 'classnames'; // for conditional class
import { connect } from 'react-redux';
import { registerUser } from '../../actions/authAction';
import PropTypes from 'prop-types';

// for  using this.props.history.push form action creator
import { withRouter } from 'react-router-dom'; 
import TextFieldGroup from '../../common/TextFieldGroup';


class Register extends Component {
  constructor(){
    super();
    this.state = {
      name: '',
      email: '',
      password: '',
      password2: '',
      errors: {}
    };
  }

  // restrict to comeback if authenticated
  componentDidMount(){
    if(this.props.auth.isAuthenticated){
      this.props.history.push('/dashboard');
    }
  }

// for recieving error
  componentWillReceiveProps(nextProps){
    if(nextProps.errors){
      this.setState({errors: nextProps.errors});
    }
  }

  onChange = (e) =>{
    this.setState({[e.target.name]: e.target.value});
  }
  onSubmit = (e) =>{
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password
    };
    console.log(newUser);
    this.props.registerUser(newUser, this.props.history);

    this.setState({
      name:'',
      email: '',
      password : '',
      password2 :''
    });
  }

  render() {
    const { user, response_msg } = this.props.auth;
    const { password, password2 } = this.state;
    return (
          <div className="register">
          <div className="container">
            <div className="row">
              <div className="col-md-8 m-auto">
                <h1 className="display-4 text-center">Sign Up</h1>
                <p className="lead text-center">Create your DevConnector account</p>
                <ul className="list-unstyled">
                  <li className="text text-danger"> 
                    {Boolean(response_msg.register) ? response_msg.register: null}
                  </li>
                </ul>
                <form onSubmit={this.onSubmit}>
                  <TextFieldGroup 
                    type="text"
                    className="form-control form-control-lg"
                    placeholder="Name"
                    name="name"
                    value={this.state.name}
                    onChange={this.onChange} 
                  />
                   <TextFieldGroup 
                    type = "email"
                    className = "form-control form-control-lg"
                    placeholder = "Email Address"
                    name = "email"
                    value = {this.state.email}
                    onChange = {this.onChange}
                    info="This site uses Gravatar so if you want a profile image, use a Gravatar email"
                  />
                  <TextFieldGroup 
                    type="password" 
                    className="form-control form-control-lg" 
                    placeholder="Password" 
                    name="password" 
                    value={this.state.password} 
                    onChange={this.onChange}
                  />
                  <TextFieldGroup 
                    type="password" 
                    className="form-control form-control-lg" 
                    placeholder="Confirm Password" 
                    name="password2" 
                    value={this.state.password2} 
                    onChange={this.onChange} 
                  />
                  <ul className="list list-unstyled">
                    <li className="text text-danger">
                      {Boolean(password !== password2) ? 'Password does not match': null}
                    </li>
                    <li className="text text-danger">
                      {Boolean(password.length < 6) ? 'Password should be at least 6 characters long': null }
                    </li>
                  </ul>
                  <input type="submit" className="btn btn-info btn-block mt-4" disabled={password !== password2 && password.length < 6}/>
                </form>
              </div>
            </div>
          </div>
        </div>

    )
  }
}

// property validation
Register.protoType = {
  auth: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
}

// map state to props
const mapStateToProps = (state) => ({
  auth: state.auth, // to access auth reducers state
  errors: state.errors
})

export default connect(mapStateToProps, { registerUser })(withRouter(Register));