import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';
import PropTypes from 'prop-types'
import ProfileActions from '../dashboard/ProfileActions';
import { addExperience } from '../../actions/profileAction';

class AddExperience extends Component {
  constructor(props){
    super(props);
    this.state = {
        title: '', // req
        company: '', // req
        location: '',
        from: '', // req
        to: '',
        current: false,
        description: '',
        disabled: false,
        errors: {}
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.errors){
      this.setState({
        errors: nextProps.errors
      })
    }
  }

  onSubmit = (e) =>{
    e.preventDefault();
    const experienceData = {
        title: this.state.title,
        company: this.state.company,
        location: this.state.location,
        from: this.state.from,
        to: this.state.to,
        current: this.state.current,
        description: this.state.description
    };
    console.log('submit clicked',experienceData);
    this.props.addExperience(experienceData, this.props.history);
  }
  
  onCheck = () =>{
    this.setState({
      disabled: !this.state.disabled,
      current: !this.state.current
    });
  }

  onChange = (e) =>{
    this.setState({[e.target.name]: e.target.value});
  }

  render() {
    return (
      <div className="add-experiences">
      <div className="container">
        <div className="row">
          <div className="col-md-8 m-auto">
            <Link to="/dashboard" className="btn btn-light">Back</Link>
            <h1 className="display-4 text-center">Add experiences</h1>
            <p className="lead text-center">Add any job or position that you had in past or current</p>
            <small className="d-block pb-3">* =required</small>
            <form onSubmit={this.onSubmit}>
            <TextFieldGroup 
              placeholder="* Company"
              name="company"
              value={this.state.company}
              className="form-control form-control-lg"
              onChange={this.onChange}
            />
            <TextFieldGroup 
              placeholder="* Title"
              name="title"
              value={this.state.title}
              className="form-control form-control-lg"
              onChange={this.onChange}
            />
            <TextFieldGroup 
              placeholder="Location"
              name="location"
              value={this.state.location}
              onChange={this.onChange}
              className="form-control form-control-lg"
            />
            <h6>From Date</h6>
            <TextFieldGroup 
              name="from"
              type="date"
              value={this.state.from}
              className="form-control form-control-lg"
              onChange={this.onChange}
            />
            <h6>To Date</h6>
              <TextFieldGroup 
              name="from"
              type="date"
              value={this.state.to}
              className="form-control form-control-lg"
              onChange={this.onChange}
              disabled={this.state.disabled ? 'disabled': ''}
            />
            <div className="form-check mb-4">
              <input 
              type="checkbox"
              name="current"
              className="form-check-input"
              value={this.state.current}
              onChange={this.onCheck}
              id="current"
              />
              <label htmlFor="current-checkbox" className="form-check-label">Current</label>
            </div>
            <TextAreaField 
              placeholder="Job description"
              name="description"
              value={this.state.description}
              onChange={this.onChange}
              className="form-control form-control-lg"
              checked={this.state.current}
            />
            <input type="submit" className="btn btn-block btn-info mt-4" value="Submit"/>
            </form>
          </div>
        </div>
      </div>
      </div>
    )
  }
}
//  validate props 
AddExperience.propTypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  addExperience: PropTypes.func.isRequired
}

// map state to props 
const mapStateToProps = (state) => ({
  auth: state.auth,
  profile: state.profile,
  error: state.errors
});
export default connect(mapStateToProps, {addExperience})(withRouter(AddExperience));