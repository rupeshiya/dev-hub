import React, { Component } from 'react';
import { Link , withRouter } from 'react-router-dom';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addEducation } from '../../actions/profileAction';

class AddEducation extends Component {
  constructor(props){
    super(props);
      this.state = {
          school: '', //req
          degree: '', // req
          fieldOfStudy: '', // req
          from: '', // req
          to: '',
          current: false,  
          description: '',
          disabled: false,
          error: {}
      }
  }
  
  onChange = (e) =>{
    this.setState({[e.target.name]: e.target.value})
  }
  
  onCheck = () =>{
    this.setState({
      current: !this.state.current,
      disabled: !this.state.disabled
    })
  }

  onSubmit = (e) => {
    e.preventDefault();
    const educationInfo = {
         school: this.state.school,
         degree: this.state.degree,
         fieldOfStudy: this.state.fieldOfStudy,
         from: this.state.from,
         to: this.state.to,
         description: this.state.description,
         current: this.state.current
    }
    this.props.addEducation(educationInfo, this.props.history);
    console.log('submitted ', educationInfo);
  }
  render() {
    return (
      <div className="add-education">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <Link to="/dashboard" className="btn btn-light">
                Go Back
              </Link>
              <h1 className="display-4 text-center">Add Your Education</h1>
              <p className="lead text-center">Add any school, bootcamp, etc that you have attended</p>
              <small className="d-block pb-3">* = required field</small>
              <form onSubmit={this.onSubmit}>
                <TextFieldGroup 
                  type = "text"
                  className = "form-control form-control-lg"
                  placeholder = "* School Or Bootcamp"
                  name = "school"
                  value={this.state.school}
                  onChange={this.onChange}
                />
                <TextFieldGroup 
                  type = "text"
                  className = "form-control form-control-lg"
                  placeholder = "* Degree Or Certificate"
                  name = "degree"
                  value={this.state.degree}
                  onChange={this.onChange}
                />
                <TextFieldGroup 
                  type = "text"
                  className = "form-control form-control-lg"
                  placeholder = "* Field of study"
                  name = "fieldOfStudy"
                  value={this.state.fieldOfStudy}
                  onChange={this.onChange}
                />
                <h6>From Date</h6>
                <TextFieldGroup 
                  type = "date"
                  className = "form-control form-control-lg"
                  name = "from"
                  value={this.state.from}
                  onChange={this.onChange}
                />
                <h6>To Date</h6>
                <TextFieldGroup 
                  type = "date"
                  className = "form-control form-control-lg"
                  name = "to"
                  value={this.state.to}
                  onChange={this.onChange}
                  disabled={this.state.disabled ? 'disabled': ''}
                />
                <div className="form-check mb-4">
                  <input 
                    className="form-check-input" 
                    type="checkbox" 
                    name="current" 
                    value=''
                    checked={this.state.current} 
                    id="current"
                    onChange={this.onCheck}
                    />
                  <label className="form-check-label" htmlFor="current">
                    Current Job
                  </label>
                </div>
                <TextAreaField
                  className = "form-control form-control-lg"
                  placeholder = "Program Description"
                  name = "description"
                  small = "Tell us about your experience and what you learned"
                  value={this.state.description}
                  onChange={this.onChange}
                />
                <input type="submit" className="btn btn-info btn-block mt-4" value="Submit" />
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
//  validate props 
AddEducation.protoTypes = {
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
}

//  map state to props 
const mapStateToProps = (state) =>({
  auth: state.auth,
  profile: state.profile,
  error: state.errors
});
export default connect(mapStateToProps, { addEducation })(withRouter(AddEducation));