import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCurrentProfile, deleteAccount } from '../../actions/profileAction';
import Spinner from '../../common/Spinner';
import { Link } from 'react-router-dom';
import ProfileActions from './ProfileActions';
import Experiences from './Experiences';
import Education from './Education';

class Dashboard extends Component {
componentDidMount(){
  this.props.getCurrentProfile();
}
onDeleteAccount = () =>{
  console.log('on delete account click');
  this.props.deleteAccount();
}
  render() {
    const { user } = this.props.auth;
    const { profile, isLoading } = this.props.profile;
    let dashboardContent;
    
    // no content came yet
    if(profile === null || isLoading){
      dashboardContent = <Spinner />
    } else {
      dashboardContent = <h1>Hello</h1>
      // check if user has profile 
      if(Object.keys(profile).length > 0){
        console.log(profile);
        dashboardContent = (
          <div>
            <h4 className="lead text-muted">Welcome <Link to={`/profile/${profile.handle}`}>
                {profile.handle}
              </Link></h4>
            <ProfileActions />
            <Experiences experience={profile.experience} />
            <Education education={profile.education} />
            <div style={{marginBottom: '60px'}}>
              <button className="btn btn-danger" onClick={this.onDeleteAccount}>Delete Account</button>
            </div>
          </div>
        )
      } else {
        // no profile yet
        dashboardContent = (
          <div>
            <h4 className="lead text-muted">Welcome {user.email} </h4>
            <p> You don 't have any profile yet, please add some info</p>
            <Link to="/create-profile" className="btn btn-lg btn-info">Create profile</Link>
          </div>
        )
      }
    }

    return (
      <div className="dashboard">
        <div className="container">
          <h1> Dashboard</h1>
          <div className="row">
            <div className="col-md-12">
              <div className="display-4">
                {dashboardContent}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// map state to props 
const mapStateToProps = (state) =>({
  profile: state.profile,
  auth: state.auth
})

export default connect(mapStateToProps, { getCurrentProfile , deleteAccount})(Dashboard);