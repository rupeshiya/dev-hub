import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { deleteEducation } from '../../actions/profileAction';
import { connect } from 'react-redux';

class Education extends Component {
  onDeleteClick = (eduId) =>{
    // console.log(expId);
    this.props.deleteEducation(eduId);
  }
  render() {
    const { education } = this.props;
    let educationCred;
    if (education !== null && education !== undefined && education.length > 0) {
       educationCred = this.props.education.map(edu => (
       <tr key={edu._id} style={{fontSize: '20px'}}>
        <td>{edu.school}</td>
        <td>{edu.degree}</td>
        <td>
          <Moment format="YYYY/DD/MM">{edu.from}</Moment> - {Boolean(edu.to) ? (edu.to) : ' Now'}
        </td>
        <td>
          <button onClick={this.onDeleteClick.bind(this, edu._id)} className="btn btn-danger">
            Delete
          </button>
        </td>
      </tr>
    ))
    }
    return (
     <div>
        <h4 className="mb-4">Education</h4>
        <table className="table">
          <thead>
            <tr style={{fontSize: '25px'}}>
              <th>School</th>
              <th>Degree</th>
              <th>Years</th>
              <th />
            </tr>
            {educationCred}
          </thead>
        </table>
      </div>
    )
  }
}
// validate props
Education.protoTypes = {
  experience: PropTypes.array.isRequired,
  deleteEducation: PropTypes.func.isRequired
}
export default connect(null, { deleteEducation })(Education);