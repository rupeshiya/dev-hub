import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import { deleteExperience } from '../../actions/profileAction';
import { connect } from 'react-redux';

class Experiences extends Component {
  onDeleteClick = (expId) =>{
    // console.log(expId);
    this.props.deleteExperience(expId);
  }
  render() {
    const { experience } = this.props;
    let experienceCred;
    if(experience !== null && experience !== undefined && experience.length > 0){
       experienceCred = this.props.experience.map(exp => (
       <tr key={exp._id} style={{fontSize: '20px'}}>
        <td>{exp.company}</td>
        <td>{exp.title}</td>
        <td>
          <Moment format="YYYY/DD/MM">{exp.from}</Moment> - {Boolean(exp.to) ? (exp.to) : ' Now'}
        </td>
        <td>
          <button onClick={this.onDeleteClick.bind(this, exp._id)} className="btn btn-danger">
            Delete
          </button>
        </td>
      </tr>
    ))
    }
    return (
     <div>
        <h4 className="mb-4">Experience </h4>
        <table className="table">
          <thead>
            <tr style={{fontSize: '25px'}}>
              <th>Company</th>
              <th>Title</th>
              <th>Years</th>
              <th />
            </tr>
            {experienceCred}
          </thead>
        </table>
      </div>
    )
  }
}
// validate props
Experiences.protoTypes = {
  experience: PropTypes.array.isRequired,
  deleteExperience: PropTypes.func.isRequired
}
export default connect(null, { deleteExperience })(Experiences);