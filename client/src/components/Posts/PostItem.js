import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { deletePost, getPosts, addLike, removeLike } from '../../actions/postAction';

class PostItem extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLiked: false
    }
  }

  onDeleteClick = (id) => {
    console.log('delete clicked', id);
    this.props.deletePost(id);
    this.props.getPosts();
  }
  onLike = (id) => {
    console.log('like clicked');
    this.props.addLike(id);
  }
  onRemoveLike = (id) => {
    console.log('unlike clicked', id);
    this.props.removeLike(id);
  }

  checkIfUserLiked = (likes) =>{
    const { auth } = this.props;
    if(likes.filter(like => like.user === auth.user.id).length > 0){
      return true
    } else {
      return false
    }
  }

  render() {
    const { post, auth, showActions } = this.props;
    return (
      <div className="card card-body mb-3">
        <div className="row">
          <div className="col-md-2">
            <a href="profile.html">
              <img className="rounded-circle d-none d-md-block" src={auth.user.avatar}
                alt="user" />
            </a>
            <br />
            <p className="text-center">{post.name}</p>
          </div>
          <div className="col-md-10">
            <p className="lead">{post.text}</p>
            {showActions ? (<span>
                 <button type="button" className="btn btn-light mr-1" onClick={this.onLike.bind(this,post._id)}>
              <i className={'fas fa-thumbs-up ' + (this.checkIfUserLiked(post.likes) ? 'text-info' : 'text-secondary') }></i>
              <span className="badge badge-light">{post.likes.length}</span>
            </button>
            <button type="button" className="btn btn-light mr-1" onClick={this.onRemoveLike.bind(this,post._id)}>
              <i className="text-secondary fas fa-thumbs-down"></i>
            </button>
            <Link to={`/post/${post._id}`} className="btn btn-info mr-1">
              Comments
            </Link>
            {Boolean(post.user === auth.user.id) ? (
              <button className="btn btn-danger mr-1" type="button" onClick={this.onDeleteClick.bind(this, post._id)}>Delete</button>
            ): null}
            </span>) : null}
          </div>
        </div>
      </div>
    )
  }
}

// validate props 
PostItem.propTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  deletePost: PropTypes.func.isRequired,
  getPosts: PropTypes.func.isRequired,
  addLike: PropTypes.func.isRequired,
  removeLike: PropTypes.func.isRequired
}
// default props 
PostItem.defaultProps = {
  showActions: true
}

// map state to props 
const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps, { deletePost, getPosts, addLike, removeLike })(PostItem);