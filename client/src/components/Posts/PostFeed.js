import React, { Component } from 'react';
import PostItem from './PostItem';
import PropTypes from 'prop-types';

class PostFeed extends Component {
  render() {
    const { posts } = this.props; 
    console.log(posts);
    let postItem;
    postItem = posts.map(post => ( // important to note that here we used '(' ')' braces to map
        <PostItem key={post._id} post={post} showActions={true} />
    ))
    return (
      <div className="post-feed">
       {postItem}
      </div>
    )
  }
}
// validate props 
PostFeed.propTypes = {
  posts: PropTypes.array.isRequired
}

export default PostFeed;