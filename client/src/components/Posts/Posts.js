import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PostForm from './PostForm';
import { getPosts } from '../../actions/postAction';
import Spinner from '../../common/Spinner';
import PostFeed from './PostFeed';

class Posts extends Component {

componentDidMount(){
  this.props.getPosts();
}

  render() {
    const { user } = this.props.auth;
    const { posts, isLoading } = this.props.post;
    console.log(posts);
    let postContent;
    if (posts === null || posts === undefined || isLoading) {
      postContent = <Spinner />
    } else {
      postContent = <PostFeed posts={posts} />
    }
    return (
      <div className="posts">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <PostForm  user={user}/>
              {postContent}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// validate props 
Posts.protoTypes = {
  profile: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired
}

//  map state to props 
const mapStateToProps = (state) => ({
  profile: state.profile,
  auth: state.auth,
  post: state.post
});

export default connect(mapStateToProps, { getPosts })(Posts);