import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addPost } from '../../actions/postAction';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';

 class PostForm extends Component {
   constructor(props){
     super(props);
     this.state = {
       text: '',
       email: '',
       avatar: '',
       errors: {}
     }
   }

   componentWillReceiveProps(nextProps){
     if(nextProps.errors){
       this.setState({errors: nextProps.errors})
     }
   }

   onSubmit = (e) => {
    e.preventDefault();
    const { email, avatar } = this.props.user;
    console.log('submitted');
    const postData = {
      text: this.state.text,
      name: email,
      avatar: avatar
    }
    this.props.addPost(postData);
    this.setState({text: ''});
   }

   onChange = (e) => {
     this.setState({[e.target.name]: e.target.value});
   }
   
  render() {

    return (
       <div className="post-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">
            Share your experience by creating a post
          </div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <TextAreaField 
                className = "form-control form-control-lg"
                placeholder = "Create a post"
                type="text"
                onChange={this.onChange}
                value={this.state.text}
                name="text"
              />  
              <button type="submit" className="btn btn-dark">Submit</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
//  validate props 
PostForm.propTypes = {
  post: PropTypes.object.isRequired,
  addPost: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
}
//  map state to props 
const mapStateToProps = (state) => ({
  post: state.post,
  error: state.errors
});
export default connect(mapStateToProps, { addPost })(PostForm);