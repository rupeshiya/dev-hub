import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CommentItem from './CommentItem';

class PostComments extends Component {
  render() {
    const { comments, postId } = this.props;
    console.log(comments);
    let commentContent;
    if(comments.length > 0){
      commentContent = comments.map(comment => (
          <CommentItem  key={comment._id} comment={comment} postId={postId}/>
      ))
    }
    return (
      <div className="comments">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              {commentContent ? (commentContent) : 'No comments yet'}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// validate props
PostComments.protoTypes = {
  comments: PropTypes.array.isRequired
}
export default PostComments;