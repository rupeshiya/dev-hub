import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addPost } from '../../actions/postAction';
import TextFieldGroup from '../../common/TextFieldGroup';
import TextAreaField from '../../common/TextAreaField';
import { addComment } from '../../actions/postAction';

 class CommentForm extends Component {
   constructor(props){
     super(props);
     this.state = {
       text: '',
       email: '',
       avatar: '',
       errors: {}
     }
   }

   componentWillReceiveProps(nextProps){
     if(nextProps.errors){
       this.setState({errors: nextProps.errors})
     }
   }

   onSubmit = (e) => {
    e.preventDefault();
    const { email, avatar } = this.props.auth;
    const { postId } = this.props;
    console.log('submitted');
    const newComment = {
      text: this.state.text,
      name: email,
      avatar: avatar
    }
    this.props.addComment(postId, newComment);
    this.setState({text: ''});
   }

   onChange = (e) => {
     this.setState({[e.target.name]: e.target.value});
   }
   
  render() {

    return (
       <div className="post-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">
            Add some comment
          </div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <TextAreaField 
                className = "form-control form-control-lg"
                placeholder = "Create a post"
                type="text"
                onChange={this.onChange}
                value={this.state.text}
                name="text"
              />  
              <button type="submit" className="btn btn-dark">Submit</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
//  validate props 
CommentForm.propTypes = {
  post: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
}
//  map state to props 
const mapStateToProps = (state) => ({
  post: state.post,
  error: state.errors,
  auth: state.auth
});
export default connect(mapStateToProps, { addComment })(CommentForm);