import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getPost } from '../../actions/postAction';
import Spinner from '../../common/Spinner';
import PostItem from '../Posts/PostItem';
import { Link } from 'react-router-dom';
import CommentForm from './CommentForm';
import PostComments from './PostComments';

class Post extends Component {
 
  componentDidMount() {
    const postId = this.props.match.params.postId;
    this.props.getPost(postId);
  }
  render() {
    const { post, isLoading } = this.props.post;
    console.log(post);
    let postContent;
    if(post === null || post === undefined || isLoading){
      postContent = <Spinner />
    } else {
      postContent = (
        <div>
          <PostItem post={post} showActions={false}/>
          <CommentForm postId={post._id} />
          <PostComments comments={post.comments} postId={post._id}/>
        </div>
      )
    }
    return (
      <div className="post">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <Link to="/posts" className="btn btn-light mb-3">Go to feeds</Link>
              {postContent}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
// validate props 
Post.protoTypes = {
  auth: PropTypes.object.isRequired,
  post: PropTypes.object.isRequired,
  getPost: PropTypes.func.isRequired
}

// map state to props 
const mapStateToProps = (state) => ({
    auth: state.auth,
    post: state.post
});
export default connect(mapStateToProps, { getPost })(Post);
