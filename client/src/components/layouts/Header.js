import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authAction';
import PropTypes from 'prop-types';
import { clearCurrentProfile, searchUser } from '../../actions/profileAction';
import { withRouter } from 'react-router-dom';
import Darkmode from 'darkmode-js';

class Header extends Component {
  constructor(props){
    super(props);
    this.state = {
      queryHandle: ''
    }
    var options = {
      bottom: '64px', // default: '32px'
      right: '32px', // default: '32px'
      left: 'unset', // default: 'unset'
      time: '0.5s', // default: '0.3s'
      mixColor: '#fff', // default: '#fff'
      backgroundColor: '#fff', // default: '#fff'
      buttonColorDark: '#100f2c', // default: '#100f2c'
      buttonColorLight: '#fff', // default: '#fff'
      saveInCookies: false, // default: true,
      label: '🌓', // default: ''
      autoMatchOsTheme: true // default: true
    }
   new Darkmode(options).showWidget();
  }

  onChange = (e) => {
    e.preventDefault();
    console.log(this.state.queryHandle);
    this.setState({ queryHandle: e.target.value });
  }

  onSearchClick = () => {
    console.log(this.state.queryHandle);
    this.props.searchUser(this.state.queryHandle, this.props.history);
  }

  render() {
    const isAuthenticated = this.props.auth.isAuthenticated;
    const user = this.props.auth.user;

    const onLogoutClick = (e) =>{
      e.preventDefault();
      console.log('logout clicked');
      this.props.clearCurrentProfile();
      this.props.logoutUser();
    }

    // links for authenticated users
    const authLinks = (
        <ul className="navbar-nav ml-auto">
           <li className="nav-item">
            <Link className="nav-link" to="/posts">Post Feed</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/dashboard">Dashboard</Link>
          </li>
          <li className="nav-item">
            <form className="form-inline my-2 my-lg-0">
              <input 
                className="form-control mr-sm-2" 
                type="search" 
                placeholder="Search" 
                aria-label="Search"
                onChange={this.onChange}
               />
              <button 
                className="btn btn-outline-success my-2 my-sm-0" type="button" onClick={this.onSearchClick}>Search</button>
            </form>
          </li>
          <li className="nav-item">
            <a href="" onClick={onLogoutClick} className="nav-link" to="/">
            <img src={user.avatar} alt={user.name} title="You should have gravatar related to your mail" style={{ width: '25px', margingRight: '5px', borderRadius: '50%'}}/>Logout</a>
          </li>
        </ul>
    );
    //  links for guests
    const guestLinks = (
      <ul className="navbar-nav ml-auto">
          {/* <li className="nav-item">
            {new Darkmode.showWidget()}
          </li> */}
          <li className="nav-item">
            <Link className="nav-link" to="/register">Sign Up</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/login">Login</Link>
          </li>
        </ul>
    );

    return (
       <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
    <div className="container">
      <Link className="navbar-brand" to="/">DevConnector</Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#mobile-nav">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="mobile-nav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/profiles"> Developers
            </Link>
          </li>
        </ul>
        {isAuthenticated ? authLinks: guestLinks}
      </div>
    </div>
  </nav>
    )
  }
}
// validate props
Header.propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  searchUser: PropTypes.func.isRequired
}

// to use isAuthenticated in this component mapStateToProps 
const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    profile: state.profile
  }
}


export default connect(mapStateToProps, { logoutUser, clearCurrentProfile, searchUser })(withRouter(Header));