import { GET_PROFILE, PROFILE_LOADING, CLEAR_CURRENT_PROFILE, GET_PROFILES, LOADING_PROFILE_PDF } from '../actions/types';

const initialState = {
  profile: null,
  profiles: null,
  isLoading: false,
  isResumeLoading: false
}

const profileReducers = (state = initialState, action) =>{
  switch(action.type){
    case PROFILE_LOADING:
      return {
        ...state,
        isLoading: true
      }
    case GET_PROFILE:
      return {
        ...state,
        profile: action.payload,
        isLoading: false
      }
    case GET_PROFILES:
      return {
        ...state,
        profiles: action.payload,
        isLoading: false
      }
    case CLEAR_CURRENT_PROFILE:
      return{
        ...state,
        profile: null,
        profiles: null
      }
    case LOADING_PROFILE_PDF: 
      return {
        ...state,
        isResumeLoading: action.payload
      }
    default:{
      return state
    }
  }
}

export default profileReducers;