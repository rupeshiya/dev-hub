import { combineReducers } from 'redux';
import authReducers from './authReducer';
import postReducers from './postReducer';
import profileReducers from './profileReducer';
import errorReducer from './errorReducer';

const rootReducer = combineReducers({
  auth: authReducers,
  post: postReducers,
  profile: profileReducers,
  errors: errorReducer
});

export default rootReducer;