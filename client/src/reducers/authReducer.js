import { TEST_REDUCERS, SET_CURRENT_USER, GET_ERROR_MSG, RESPONSE_MSG } from "../actions/types";
// auth reducers
const intialState = {
  isAuthenticated: false,
  user: {},
  response_msg: {}
};

// reducer
export default function(state = intialState, action){
  switch(action.type){
    case TEST_REDUCERS:
      return{
        ...state,
        user: action.payload
      }
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: Boolean((action.payload && typeof action.payload === 'object' && Object.keys(action.payload).length !== 0)),
        user: action.payload
      }
    case RESPONSE_MSG: 
      return {
        ...state,
        response_msg: action.payload
      }
    default:{
      return state
    }
  }
}