import { GET_POST, GET_POSTS, ADD_POST, POST_LOADING, DELETE_POST } from "../actions/types";

const initialState = {
  post: {},
  posts: [],
  isLoading: true
}


// post reducers 
export default function(state = initialState, action){
  switch(action.type){
    case POST_LOADING:
      return {
        ...state,
        isLoading: true
      }
    case GET_POST:
      return {
        ...state,
        post: action.payload,
        isLoading: false
      }
    case GET_POSTS:
      return {
        ...state,
        posts: action.payload,
        isLoading: false
      }
    case ADD_POST:
      return {
        ...state,
        posts: [action.payload, ...state.posts]
      }
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post._id !== action.payload)
      }
    default:
      return state
  }
}