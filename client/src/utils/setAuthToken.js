import axios from 'axios';

export const setAuthToken = (token) =>{
  if(token){
    console.log('set auth token in header called ', token);
    // set in header i.e AUTHORIZATION
    axios.defaults.headers.common['Authorization'] = token; // to all the request
  } else {
    // if not token i.e not logged in
    delete axios.defaults.headers.common['Authorization']
  }
}