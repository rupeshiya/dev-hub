import React from 'react';
import PropTypes from 'prop-types'

const TextFieldGroup = ({
    placeholder, name, value, onChange, type, className, disabled, info, error
  }) => {
  return (
      <div className="form-group">
        <input type={type} className={className} placeholder={placeholder} name={name} value={value} onChange={onChange} info={info} disabled={disabled} required/>
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <small className="form-text text-muted">{error}</small>}
      </div>
  )
}
// validate props
TextFieldGroup.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  info: PropTypes.string,
}
// defaulty value
TextFieldGroup.defaultProps = {
  type: 'text'
};

export default TextFieldGroup;