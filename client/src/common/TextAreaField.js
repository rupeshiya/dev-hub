import React from 'react';
import PropTypes from 'prop-types';

function TextAreaField({info, placeholder, type, name, value, onChange, className, error }) {
  return (
     <div className="form-group">
        <textarea type={type} className={className} placeholder={placeholder} name={name} value={value} onChange={onChange} info={info} required row="10" column="7"/>
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <small className="form-text text-muted">{error}</small>}
      </div>
  )
}

TextAreaField.protoType = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  info: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

export default TextAreaField;