import React from 'react';
import PropTypes from 'prop-types';

const SocialInputGroup = ({
    placeholder, name, value, onChange, type, className, disabled, icon, error
  }) => {
  return (
      <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text">
          <i className={icon} />
        </span>
      </div>
      <input
        className={className}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
      />
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  )
}
// validate props
SocialInputGroup.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  icon: PropTypes.string,
}
// defaulty value
SocialInputGroup.defaultProps = {
  type: 'text'
};

export default SocialInputGroup;