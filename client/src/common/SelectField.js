import React from 'react';
import PropTypes from 'prop-types';

function SelectField({ value, className, error, info, options, onChange, name}) {
  // option is an array of objects having label and value
  const selectOptions = options.map(option => (
    <option key={option.label} value={option.value}>{option.label}</option>
  ));

  return (
    <div className="form-group">
    <select className={className} value={value} onChange={onChange} name={name} >
      {selectOptions}
    </select>
    {info && <small className="form-text text-muted">{info}</small>}
    {error && <small className="invalid-feedback">{error}</small>}
  </div>
  )
}

SelectField.protoType = {
  name: PropTypes.string.isRequired,
  info: PropTypes.string,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired
}
export default SelectField;