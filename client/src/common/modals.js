import React, { Component } from 'react';
import { hireDeveloper } from '../actions/profileAction';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {NotificationContainer, NotificationManager} from 'react-notifications';

class Modal extends Component {
  constructor(props){
    super(props);
    this.state = {
      senderName: '',
      message: ''
    }
  }

  onChange = (e) => {
    e.preventDefault();
    this.setState({[e.target.name]: e.target.value });
  }

  onSendClick = (profileId) => {
    // e.preventDefault();
    const hiringInfo = {
      senderName: this.state.senderName,
      message: this.state.message
    };
    console.log(hiringInfo);
    this.props.hireDeveloper(hiringInfo, profileId);
  }

  // component will receive props 
  componentWillReceiveProps(nextprops) {
    console.log(nextprops);
    if(nextprops.auth.response_msg){
      NotificationManager.success('Your request has been sent successfully', 'Hire request')
      // notificationontent = (
      //   <div>
      //     <NotificationContainer />
      //   </div>
      // )
    }
  }

  render() {
    const { profileId  } = this.props;
    const { response_msg } = this.props.auth;
    return (
        <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Hire the best!</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>
                              <span className="modal-lable">Sender Name:</span>
                              <input value={this.state.senderName} 
                              onChange={this.onChange}
                              className = "form form-control"
                              type="text"
                              name="senderName"
                              required
                              />
                            </p>
                            <p>
                              <span className="modal-lable">Message:</span>
                              <textarea value={this.state.message} 
                              onChange={this.onChange} 
                              name="message"
                              type="text"
                              rows="5"
                              className="form form-control"
                              required
                              />
                            </p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.onSendClick.bind(this, profileId)} disabled={!this.state.senderName || !this.state.message }>Send</button>
                            {/* add notification on success */}
                        </div>
                        {/* {Boolean(this.state.isSent) ? NotificationManager.success('Your request has been sent successfully', 'Hire request') : NotificationManager.error('Error in sending request ', 'Opps!')}
                        <NotificationContainer/>
                        { console.log(response_msg)} */}
                        {/* {Boolean(notificationontent) ? notificationontent : null} */}
                    </div>
                </div>
            </div>
    )
  }
}
// validate props 
Modal.propTypes = {
  profileId: PropTypes.string.isRequired,
  hireDeveloper: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

// map state to props 
const mapStateToProps = ( state ) => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps, { hireDeveloper })(Modal);
