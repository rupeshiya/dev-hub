import axios from 'axios';
import { GET_POSTS, GET_ERRORS, ADD_POST, GET_POST, DELETE_POST, POST_LOADING, ADD_COMMENT, DELETE_COMMENT } from './types';

// GET POST
//  async function insideGetPosts(){
//   const posts = await axios.get('/api/posts');
//   return posts;
// }

// GET POSTS 
export const  getPosts = () => dispatch => {
  dispatch(setPostLoading());
  // const res = insideGetPosts()
  axios.get('/api/posts')
    .then((res)=>{
      console.log(res);
      dispatch({
        type:GET_POSTS,
        payload: res.data.posts
      })
    })
    .catch((err) => {
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

//  add posts 
export const addPost = (postData) => dispatch => {
  axios.post('/api/posts', postData)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: ADD_POST,
        payload: res.data.post
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// GET POST
export const getPost = (postId) => dispatch => {
  dispatch(setPostLoading());
  axios.get(`/api/posts/${postId}`)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: GET_POST,
        payload: res.data.post
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

//  delete post 
export const deletePost = (postId) => dispatch => {
  axios.delete(`/api/posts/${postId}`)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: DELETE_POST,
        payload: postId
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// add like 
export const addLike = (id) => dispatch => {
  axios.post(`/api/posts/like/${id}`)
    .then((res)=>{
      console.log(res);
      dispatch(getPosts());
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// add dislike 
export const removeLike = (id) => dispatch => {
  axios.post(`/api/posts/unlike/${id}`)
    .then((res)=>{
      console.log(res)
      dispatch(getPosts());
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// add comment
export const addComment = (id, comment) => dispatch => {
  axios.post(`/api/posts/comment/${id}`, comment)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: GET_POST,
        payload: res.data.post
      })      
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// delete comment
export const deleteComment = (postId, commentId) => dispatch => {
  axios.delete(`/api/posts/comment/${postId}/${commentId}`)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: GET_POST,
        payload: res.data.post
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// set post loading
export const setPostLoading = () => {
  return {
    type: POST_LOADING
  }
}