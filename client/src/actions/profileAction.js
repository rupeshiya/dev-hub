import axios from 'axios';
import { GET_PROFILE, PROFILE_LOADING, CLEAR_CURRENT_PROFILE, GET_ERRORS, SET_CURRENT_USER, GET_PROFILES, RESPONSE_MSG, LOADING_PROFILE_PDF } from './types';


export const getCurrentProfile = () => (dispatch) =>{
  dispatch(setProfileLoading());
  axios.get('/api/profile')
    .then((res)=>{
      console.log('fetched profile ', res);
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      })
    })
    .catch((err)=>{
      console.log(err);
      dispatch({
        type: GET_PROFILE,
        payload: {}
      })
    });
}

// create profile 
export const createProfile = (profileData, history) => dispatch =>{
  axios.post('/api/profile', profileData)
    .then((res)=>{
      console.log(res);
      history.push('/dashboard');
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// delete account and profile
export const deleteAccount = () => dispatch =>{
  if(window.confirm('Are you sure ?')){
    axios.delete('/api/profile')
      .then((res)=>{
        dispatch({
          type: SET_CURRENT_USER,
          payload: {}
        })
      })
      .catch((err)=>{
        dispatch({
          type: GET_ERRORS,
          payload: err
        })
      })
  }
}

// add experience
export const addExperience = (experienceInfo, history) => dispatch =>{
  axios.post('/api/profile/experience', experienceInfo)
    .then((res)=>{
      console.log(res);
      history.push('/dashboard');
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      });
    })
}

// add education
export const addEducation = (educationInfo, history) => (dispatch) =>{
  axios.post('/api/profile/education', educationInfo)
    .then((res)=>{
      console.log(res);
      history.push('/dashboard');
    })
    .catch((Err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: Err
      })
    })
}

// delete experiences
export const deleteExperience = (id) => dispatch =>{
  axios.delete(`/api/profile/experience/${id}`)
    .then((res)=>{
      console.log('deleted ');
      window.location.reload();
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// delete education
export const deleteEducation = (eduId) => (dispatch) => {
  axios.delete(`/api/profile/education/${eduId}`)
    .then((res)=>{
      window.location.reload();
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// get profiles
export const getProfiles = () => dispatch => {
  dispatch(setProfileLoading());
  axios.get('/api/profile/all')
    .then((res)=>{
      console.log(res.data);
      dispatch({
        type: GET_PROFILES,
        payload: res.data
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_PROFILES,
        payload: null
      })
    })
}

// get profile by handle 
export const getProfileByHandle = (handle) => dispatch => {
  dispatch(setProfileLoading());
  axios.get(`/api/profile/handle/${handle}`)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: GET_PROFILE,
        payload:res.data
      })
    })
    .catch((err)=>{
      dispatch({
        type: GET_PROFILE,
        payload: null
      })
    })
}

// add profile pic
export const uploadImage = (image) => dispatch => {
  axios.post('/api/profile/profilePic', image)
    .then((res)=>{
      console.log(res);
      dispatch({
        type: GET_PROFILE,
        payload: res.data.profile
      })
    })
    .catch((err)=>{
      console.log(err);
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

// hire the best 
export const hireDeveloper = (hiringInfo, profileId) => (dispatch) => {
  axios.post(`/api/profile/hire/message/${profileId}`, hiringInfo)
    .then((res)=>{
      console.log(res);
      if(res.data.success){
        dispatch({
          type: RESPONSE_MSG,
          payload: res.data.success
        })
      } else {
        dispatch({
          type: RESPONSE_MSG,
          payload: res.data
        })
      }
    })
    .catch((err)=>{
      console.log(err);
      dispatch({
        type: GET_ERRORS,
        payload: err
      });
    })
}

// implement search functionality 
export const searchUser = (handle, history) => dispatch => {
  const url = `/api/profile/user/search?handle=${handle}`;
  axios.get(url, handle)
  .then((response)=>{
    console.log(response);
    dispatch({
      type: GET_PROFILE,
      payload: response.data.profile
    });
    history.push(`/profile/${handle}`);
  })
  .catch((err)=>{
    console.log(err);
    dispatch({
      type: GET_PROFILE,
      payload: {}
    });
  });
}

// get profile as pdf 
// for spinner
export const setProfileLoading = () =>  {
  return {
    type: PROFILE_LOADING
  }
}

// clear profile after logout
export const clearCurrentProfile = () => {
  return {
    type: CLEAR_CURRENT_PROFILE
  }
}

// for spinner 
export const setProfilePdfLoading = () => {
  return {
    type: LOADING_PROFILE_PDF
  }
}