import { TEST_REDUCERS, GET_ERRORS, SET_CURRENT_USER, RESPONSE_MSG } from './types';
import axios from 'axios';
import { setAuthToken } from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

export const registerUser = (userInfo, history) => (dispatch) => {
  const response_msg = {};
  axios.post('/api/users/register', userInfo)
    .then((res)=>{
      console.log(res.data);
      if(res.data.success){
        history.push('/login');
      } else {
        response_msg.register = res.data.msg
        dispatch({
          type: RESPONSE_MSG,
          payload: response_msg
        });
      }
    })
    .catch((err)=>{
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    })
}

export const loginUser = (userInfo) => (dispatch) => {
  axios.post('/api/users/login', userInfo)
    .then((res) => {
      console.log(res.data);
      const token = res.data.token;
      //  save token into localStorage
      localStorage.setItem('jwtToken', token);
      //  set token in header
      setAuthToken(token);
      
      //  decode token to get user data
      const decodedData = jwt_decode(token);
      console.log(decodedData);

      // set current user 
      dispatch(setCurrentUser(decodedData));
    })
    .catch((err) => {
      let error_msg = {
        msg: 'Incorrect username or password'
      }
      dispatch({
        type: GET_ERRORS,
        payload: error_msg
      })
    })
}

//  set current user
export const setCurrentUser = (decodedData) => {
  return {
    type: SET_CURRENT_USER,
    payload: decodedData
  }
}

// logoutUser
export const logoutUser = () => dispatch => {
  // remove token from local storage
  localStorage.removeItem('jwtToken');
  // remove header  authorization
  setAuthToken(false);
  // set current user to empty
  dispatch(setCurrentUser({}));
}