const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateEducationInput(data){
  let errors = {};
  
  data.school = Boolean(data.school) ? data.school : '';
  data.degree = Boolean(data.degree) ? data.degree : '';
  data.fieldOfStudy = Boolean(data.fieldOfStudy) ? data.fieldOfStudy : '';
  data.from = Boolean(data.from) ? data.from : '';

  if(validator.isEmpty(data.school)){
    errors.school = 'School field is required';
  }
  if (validator.isEmpty(data.degree)) {
    errors.degree = 'Degree field is required';
  }
  if (validator.isEmpty(data.fieldOfStudy)) {
    errors.fieldOfStudy = 'Filed of study is required';
  }
  if (validator.isEmpty(data.from)) {
    errors.from = 'From is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}