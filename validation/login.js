const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateLoginInput(data) {
  let errors = {};
  // since validator only check for string as an argument
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  
  if (validator.isEmpty(data.email)) {
    errors.email = "Email is required"
  }

  if (validator.isEmpty(data.password)) {
    errors.password = "Password is required"
  }
  if (!validator.isEmail(data.email)) {
    errors.email = "Invalid email";
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  }
}