const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateExperineceInput(data){
  let errors = {};

  // data.company = !isEmpty(data.company) ? data.company : '';
  // data.title =  !isEmpty(data.title) ? data.title : '';
  // data.from = !isEmpty(data.from) ? data.from : '';
  data.company = Boolean(data.company) ? data.company : '';
  data.title = Boolean(data.title) ? data.title : '';
  data.from = Boolean(data.from) ? data.from : '';

  if(validator.isEmpty(data.company)){
    errors.company = 'Company name is required';
  }
  if(validator.isEmpty(data.title)){
    errors.title = 'Title is required';
  }
  if(validator.isEmpty(data.from)){
    errors.from = 'From field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}