const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validatePostInput(data){
  let errors  = {};
  data.text = Boolean(data.text) ? data.text : '';

  if(!validator.isLength(data.text, {min: 10, max: 300})){
    errors.text = 'text should be between 10 and 300 characters';
  }
  if(validator.isEmpty(data.text)){
    errors.text = 'text is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}