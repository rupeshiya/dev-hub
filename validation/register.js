const validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports = function validateRegisterInput(data) {
  let errors = {};
  // since validator only check for string as an argument
  data.name = !isEmpty(data.name) ? data.name : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : ''; 

  // check for length
  if(!validator.isLength(data.name  , { min: 2, max: 30})){
    errors.name = "Name must be in between 2 and 30 characters long";
  }
  //  check for empty field
  if(validator.isEmpty(data.name)){
    errors.name = "Name is required"
  }

  if(!validator.isEmail(data.email)){
    errors.email = "Invalid email";
  }

  if (validator.isEmpty(data.email)) {
    errors.email = "Email is required"
  }

  if (validator.isEmpty(data.password)) {
    errors.name = "Password is required"
  }

  if(!validator.isLength(data.password, {min: 6, max: 13})){
    errors.password = 'Password should be at least 6 characters long';
  }

  // match password
  if(!validator.equals(data.password, data.password2)){
    errors.password2 = "Password does not match";
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  }
}