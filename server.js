require('dotenv').config()
const express = require('express');
const app = express();
const keys = require('./config/keys');
const port = process.env.PORT || keys.port;
const mongoose = require('mongoose');
const users = require('./Routes/api/users');
const profile = require('./Routes/api/profile');
const posts = require('./Routes/api/posts');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const compressor = require('compression');
const morgan = require('morgan');
const winston = require('./config/winston')

// mongoUri = 'mongodb://rupesh:abc123@ds249717.mlab.com:49717/devconnector'

//  connect mongo
mongoose.connect(process.env.mongoUri, {
    useNewUrlParser: true
  })
  .then(() => {
    console.log('mongo connected');
  })
  .catch((err) => {
    console.log('mongo error',err);
  });

// compress response
app.use(compressor());

// passport middleware
app.use(passport.initialize());
// passport config
require('./config/passport')(passport);

// enable cors
app.use(cors());

// middleware
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '/client/build')));
app.use('/uploads',express.static(path.join(__dirname, 'uploads/')));
app.use(
  morgan(
    ':remote-addr - :remote-user [:date[clf]] ":method :url" :status :res[content-length] ":referrer" ":user-agent" :data', {
      stream: winston.stream
    }
  )
)

//  test route
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/client/build/index.html'));
});

// use routers
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/posts', posts);

app.listen(port, () => {
  console.log(`listening on ${port}`);
});
// https://affectionate-bohr-3d9547.netlify.com/
// https://sleepy-beach-78441.herokuapp.com/